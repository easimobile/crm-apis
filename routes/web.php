<?php
Route::get('/',array('middleware' => 'userSession', function () {
    return redirect('login');
}));

Route::any('login',array('middleware' => 'userSession','uses'=>'UserController@login'));
Route::get('logout',array('uses'=>'UserController@logout'));
Route::any('changeExpiredPassword',array('middleware' => 'userSession','uses'=>'UserController@changeExpiredPassword'));
Route::any('resetPassword',array('middleware' => 'userSession','uses'=>'UserController@resetPassword'));

Route::group(array('prefix'=>'User','namespace'=>'Api'), function(){
    Route::post('getUserInformation',array('uses'=>'UserApiController@getUserInformation'));
    Route::post('verifyUser',array('uses'=>'UserApiController@verifyUser'));
    Route::post('login',array('uses'=>'UserApiController@login'));
    Route::post('register',array('uses'=>'UserApiController@register'));
    Route::post('updateUserProfile',array('uses'=>'UserApiController@updateUserProfile'));
    Route::post('logout',array('uses'=>'UserApiController@logout'));
    Route::post('getInvitationInfo',array('uses'=>'UserApiController@getInvitationInfo'));
    Route::post('faceRegister',array('uses'=>'UserApiController@faceRegister'));
    Route::post('faceLogin',array('uses'=>'UserApiController@faceLogin'));
    Route::post('removeFaceLogin',array('uses'=>'UserApiController@removeFaceLogin'));
});

Route::group(array('prefix'=>'Event','namespace'=>'Api'), function(){
    Route::post('getEvents',array('uses'=>'MobileApiController@getEvents'));
    Route::post('getEventDetails',array('uses'=>'MobileApiController@getEventDetails'));

});

Route::group(array('prefix'=>'History','namespace'=>'Api'), function(){
    Route::post('getTransactionHistory',array('uses'=>'MobileApiController@getTransactionHistory'));
    Route::post('getPointHistory',array('uses'=>'MobileApiController@getPointHistory'));
});

Route::group(array('prefix'=>'Reward','namespace'=>'Api'), function(){
    Route::post('getRewards',array('uses'=>'MobileApiController@getRewards'));
    Route::post('getRewardDetails',array('uses'=>'MobileApiController@getRewardDetails'));
    Route::post('getUserActiveRewards',array('uses'=>'MobileApiController@getUserActiveRewards'));
    Route::post('getUserPastRewards',array('uses'=>'MobileApiController@getUserPastRewards'));
    Route::post('getUserRewardDetails',array('uses'=>'MobileApiController@getUserRewardDetails'));
    Route::post('redeemReward',array('uses'=>'MobileApiController@redeemReward'));
    Route::post('claimMyReward',array('uses'=>'MobileApiController@claimMyReward'));
});

Route::group(array('prefix'=>'Notification','namespace'=>'Api'), function(){
    Route::post('sendNotification',array('uses'=>'MobileApiController@sendNotification'));
    Route::post('registerPushToken',array('uses'=>'MobileApiController@registerPushToken'));
    Route::post('removePushToken',array('uses'=>'MobileApiController@removePushToken'));
    Route::post('getNotification',array('uses'=>'MobileApiController@getNotification'));
    Route::post('dismissNotification',array('uses'=>'MobileApiController@dismissNotification'));
    Route::post('_sendNotification',array('uses'=>'MobileApiController@_sendNotification'));
});

Route::group(array('prefix'=>'Kiosk','namespace'=>'Api'), function(){
    Route::post('Transactions',array('uses'=>'KioskApiController@Transactions'));
    Route::post('TransactionsSyncByFace',array('uses'=>'KioskApiController@TransactionsSyncByFace'));
});

Route::group(array('middleware' => ['userAuth','web']), function(){
	Route::get('contactUs',array('uses'=>'UserController@contactUs'));
    Route::any('changePassword',array('uses'=>'UserController@changePw'));
    Route::group(array('prefix'=>'Configuration'),function(){
        Route::get('Event',array('as'=>'Event','uses'=>'EventController@event'));
        Route::get('Reward',array('as'=>'Reward','uses'=>'RewardController@reward'));
    });
});

Route::group(array('prefix' => 'api'), function(){
    Route::post('login',array("uses"=>"ApiController@login"));
    // Route::post('saveAuditLog',array('uses'=>'ApiController@saveAuditLog'));
	Route::post('changePassword',array("uses"=>"ApiController@changePw"));
    Route::post('resetPassword',array('uses'=>"ApiController@resetPassword"));
    Route::get('getSysflagValue',array('uses'=>'ApiController@getSysflagValue'));

    Route::group(array('prefix'=>'Event','namespace'=>'Api'),function(){
		Route::get('getEvents',array("uses"=>"EventApiController@getEvents"));
		Route::post('addEvent',array("uses" =>"EventApiController@addEvent"));
		Route::post('deleteEvent',array("uses" =>"EventApiController@deleteEvent"));
        Route::post('editEvent',array("uses"=>"EventApiController@editEvent"));
        Route::get('getEventTypes',array('uses'=>'EventApiController@getEventTypes'));
    });

    Route::group(array('prefix'=>'Reward','namespace'=>'Api'),function(){
		Route::get('getRewards',array("uses"=>"RewardApiController@getRewards"));
		Route::post('addReward',array("uses" =>"RewardApiController@addReward"));
        Route::post('editReward',array("uses"=>"RewardApiController@editReward"));
	});
});

Route::group(array('middleware' => 'apiAuth'), function(){
    //get user info
    Route::get('getUserInfo',array('uses'=>'ApiController@getUserInfo'));
});
