@extends('layouts.master')
@section('content')
<style>
#saveEditedRewardBtn,#addRewardBtn, #closeModal{
  float: right;
  display: inline-block;
  margin: 1px;
}

.editRewardButton, .viewRewardButton, .copyRewardButton, .deleteRewardButton{
  padding: 2px 7px;
  margin: 1px;
}

.addBtn-Reward{
  width: 100%;
  text-align: right;
  padding: 5px;
   display: inline-block;
}
</style>

<div class="overlay">
	<div id="loading-img"></div>
</div>
<div class="box-header">
    <h3 class="page-header">Rewards</h3>
</div>
@if($userFeatureFlag->AllowAdd == 1)
<div class="addBtn-Reward" id="openAddRewardDiv">
  <button type="button" class="btn btn-primary" id="openAddRewardBtn"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
</div>
@endif
<div class="box-body" id="boxBody">
    <div id="loader" style="display:none;"></div>
    <table id="rewardTbl" class="table table-bordered table-hover">
      <thead>
        <tr>
          <th>Reward Name</th>
          <th>Short Description</th>
          <th>Start Date</th>
          <th>Type</th>
          <th>Active</th>
          <th></th>
        </tr>
      </thead>
      <tbody id="sortable" style="text-align:center;">
      </tbody>
    </table>
</div>

<div class="modal fade" id="rewardModal" tabindex="-1" role="dialog" aria-labelledby="rewardModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="closeModal">Close</button>
            <button type="button" class="btn btn-primary" id="saveEditedRewardBtn">Save</button>
            <button type="button" class="btn btn-primary" id="addRewardBtn">Add</button>
            <h4 class="modal-title" id="rewardModalTitle" style="text-overflow: ellipsis;overflow: hidden;"></h4>
        </div>
        <div class="modal-body">
            <form id="rewardForm" method="POST">
            <div class="checkbox">
                <label>
                    <input type="checkbox" id="activeCheckbox" name="active" value="1"> Active
                </label>
            </div>
            <div class="form-group">
                <label for="code">Reward Name</label>
                <input type="text" class="form-control" id="rewardName" name="rewardName" maxlength="500">
            </div>
            <div class="form-group">
                <label for="paymentType">Type</label>
                <select class="form-control" id="type" name="type">
                    <option value="LIMITED">Limited</option>
                    <option value="SPECIAL">Special</option>
                </select>
            </div>
            <div class="form-group">
                <label for="RewardTitle">Start Date</label>
                <input type="datetime-local" class="form-control" id="startDate" name="startDate"/>
            </div>
            <div class="form-group">
                <label for="RewardTitle">End Date</label>
                <input type="datetime-local" class="form-control" id="endDate" name="endDate"/>
            </div>
            <div class="form-group">
                <label for="code">Reward Points</label>
                <input type="number" class="form-control" id="pointsToRedeem" name="pointsToRedeem">
            </div>
            <div class="form-group">
                <label for="code">Max Redeem Per Reward <small style="font-weight: 100;">(Please insert -1 for unlimited redeem)</small></label>
                <input type="number" class="form-control" id="maxRedeemedPerReward" name="maxRedeemedPerReward">
            </div>
            <div class="form-group">
                <label for="code">Max Redeem Per User <small style="font-weight: 100;">(Please insert -1 for unlimited redeem)</small></label>
                <input type="number" class="form-control" id="maxRedeemedPerUser" name="maxRedeemedPerUser">
            </div>
            <div class="form-group">
                <label for="Location">Location</label>
                <input type="text" class="form-control" id="location" name="location" maxlength="100">
            </div>
            <div class="form-group">
                <label for="image">Image <small style="font-weight: 100;">(maximum {{ (int)($maxFileSize/1000) }}MB file size)</small></label>
                <input type="file" id="image" name="image" accept="image/jpg,image/jpeg,image/png">
                <a href="#" id="imageViewAnchor" data-lightbox="imageView">
                    <img id="imageView" class="imageView" width="auto" height="60px"/>
                </a>
            </div>
            <div class="form-group">
                <label for="rewardShortDescription">Short Description</label>
                <textarea class="form-control" id="rewardShortDescription" name="rewardShortDescription"rows="3"></textarea>
            </div>
            <div class="form-group">
                <label for="rewardLongDescription">Long Description</label>
                <textarea class="form-control" id="rewardLongDescription" name="rewardLongDescription"rows="5"></textarea>
            </div>
            <div class="form-group">
                <label for="rewardTnc">Terms and Condition</label>
                <textarea class="form-control" id="rewardTnc" name="rewardTnc" rows="5"></textarea>
            </div>

            <input type="hidden" id="rewardId" name="rewardId">
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
var APP_URL = {!! json_encode(url('/')) !!};
var rewardTbl;
var apiKey = '<?php echo $apiKey; ?>';

var userFeatureFlag = '<?php echo json_encode($userFeatureFlag); ?>';
userFeatureFlag = JSON.parse(userFeatureFlag);

getDataTable();

$("#rewardTbl").DataTable({
  "searching": true,
  "order": [],
  "data" : [],
  "columns": [
    { data: 'rewardName'},
    { data: 'rewardShortDescription'},
    { data: 'startDate', render: function(data, type, full){
      if(data){
        data = data.split('.');
        return data[0];
      }
      else
        return '-';
    }},
    { data: 'type'},
    { data: 'isActive', render: function(data, type, full){
        var bullet = '•';
        if(data == 1)
            return bullet.fontcolor("green").fontsize(5);
        if(data == 0)
            return bullet.fontcolor("red").fontsize(5);
    }},
    { data: 'rewardId',render: function(data,type,full){
        var string = '';

        string += '<button class="btn btn-success viewRewardButton" title="View" name="viewRewardButton"><i class="fa fa-eye" aria-hidden="true"></i></button>';

        if(userFeatureFlag['AllowEdit'] == 1){
            string += '<button class="btn btn-warning editRewardButton" title="Edit" name="editRewardButton"><i class="glyphicon glyphicon-edit"></i></button>';
        }

        if(userFeatureFlag['AllowAdd'] == 1){
            string += '<button class="btn btn-primary copyRewardButton" title="Copy" name="copyRewardButton"><i class="fa fa-files-o" aria-hidden="true"></i></button>';
        }

        if(string == '')
            return '-';
        else
            return string;
    }}],
    "fnRowCallback" : function(nRow, aData, iDisplayIndex, iDisplayIndexFull){
      if(aData['active'] == 0){
        $('td', nRow).css('background-color', 'rgba(211, 211, 211, 0.63)');
      }
    }
});

$("#startDate").change(function(){
    var defaultDate = $('#startDate').val();
    $('#endDate').val(defaultDate);
});

$('#openAddRewardBtn').click(function(reward){
    //open modal
    $("#rewardForm :input").prop("disabled", false);
    $("#rewardForm :input").prop("readonly", false);
    $('#saveEditedRewardBtn').css({'display': 'none'});
    $('#editRewardBtn').css({'display': 'none'});
    $('#addRewardBtn').css({'display': 'inline-block'});
    $('#rewardForm')[0].reset();
    $('#imageView').attr({'src':APP_URL+'/images/noImage.png'});
    $('#imageViewAnchor').attr({'href':APP_URL+'/images/noImage.png'});
    $('#rewardModalTitle').text('Add New Reward');
    $('#addRewardBtn').data('copy-code','');

    $('#rewardModal').modal('toggle');
});

$(function(){
  $(document).on('click', 'button[name="viewRewardButton"]', function(){
    $('#addRewardBtn').css({'display': 'none'});
    $('#saveEditedRewardBtn').css({'display': 'none'});
    var row = $(this).closest('tr');
    var data = $('#rewardTbl').DataTable().row(row).data();
    fillData(data);
    // saveAudit(moduleName,data.code,'View',apiKey);
    $('#rewardModalTitle').text(data.reward);
    $("#rewardForm :input").prop("disabled", true);
  })
});

$(function(){
  $(document).on('click', 'button[name="editRewardButton"]', function(){
    $('#addRewardBtn').css({'display': 'none'});
    $('#saveEditedRewardBtn').css({'display': 'inline-block'});
    var row = $(this).closest('tr');
    var data = $('#rewardTbl').DataTable().row(row).data();
    fillData(data);
    $('#rewardModalTitle').text('Reward ID: '+data.rewardId);
    $("#rewardForm :input").prop("disabled", false);
  })
});

/* $(function(){
  $(document).on('click', 'button[name="deleteRewardButton"]', function(){
    var id = $(this).data('type-code');
    $('#confirmDelete').data('id', id);
    $("#confirmDelete").dialog("open");
  })
}); */

$(function(){
  $(document).on('click', 'button[name="copyRewardButton"]', function(){
    $('#addRewardBtn').css({'display': 'inline-block'});
    $('#saveEditedRewardBtn').css({'display': 'none'});
    var row = $(this).closest('tr');
    var data = $('#rewardTbl').DataTable().row(row).data();
    fillData(data);
    $('#rewardModalTitle').text('Add New Reward');
    $("#rewardForm :input").prop("disabled", false);
  })
});

$('#image').on("change", function(){
  if($(this)[0].files[0]){
    var reader = new FileReader();
    reader.onload = function (e) {
        $('#imageView').attr('src', e.target.result);
        $('#imageViewAnchor').attr('href', e.target.result);
    }
     reader.readAsDataURL($(this)[0].files[0]);
  }
});

function fillData(data){
    $('#rewardId').val(data.rewardId);
    $('#rewardName').val(data.rewardName);
    $('#rewardShortDescription').val(data.rewardShortDescription);
    $('#rewardLongDescription').val(data.rewardLongDescription);
    $('#rewardTnc').val(data.rewardTnc);
    $('#location').val(data.location);
    $('#type').val(data.type);
    $('#addRewardBtn').data('copy-code', data.rewardId);
    $('#maxRedeemedPerReward').val(data.maxRedeemedPerReward);
    $('#maxRedeemedPerUser').val(data.maxRedeemedPerUser);
    $('#pointsToRedeem').val(data.pointsToRedeem);
    // $('#createdAt').val(data.createdAt);
    $('#startDate').val(moment(data.startDate).format('YYYY-MM-DDTHH:mm:ss'));
    $('#endDate').val(moment(data.endDate).format('YYYY-MM-DDTHH:mm:ss'));

    if(data.isActive == 1)
        $("#activeCheckbox").prop('checked', true);
    else
        $("#activeCheckbox").prop('checked', false);

    if(data.imageInBase64){
        $('#imageViewAnchor').attr({'href':'data:image/jpeg;base64,'+ data.imageInBase64});
        $('#imageView').attr({'src':'data:image/jpeg;base64,'+ data.imageInBase64});
    }else{
        $('#imageView').attr({'src':APP_URL+'/images/noImage.png'});
        $('#imageViewAnchor').attr({'href':APP_URL+'/images/noImage.png'});
    }

    $('#rewardModal').modal('toggle');
}


$('#addRewardBtn').click(function(reward){
    addReward();
});

$('#saveEditedRewardBtn').click(function(reward){
    editReward();
});

function addReward(){
  //add
  var copyCode = $('#addRewardBtn').data('copy-code');
  $('.overlay').css({'display':'block'});
  var formData = new FormData($('#rewardForm')[0]);
  formData.append('apiKey',apiKey);
  formData.append('copyCode',copyCode);
  $.ajax({
      type: "POST",
      url: APP_URL+"/api/Reward/addReward",
      data: formData,
      contentType: false,
      cache: false,
      processData:false,
      success: function(data)
      {
        if(data.error == false){
          $('#rewardModal').modal('toggle');
          //save to audit
          var id = $('#code').val();
          //saveAudit(moduleName,id,'Add',apiKey);
          alertDialog(data.message);
          getDataTable();
        }else
          alert(data.message);
        $('.overlay').css({'display':'none'});
      },error: function(){
        alert('Problem occur. Please try again');
        $('.overlay').css({'display':'none'});
      }
  });
}

function editReward(){
  //edit
  $('.overlay').css({'display':'block'});
  var formData = new FormData($('#rewardForm')[0]);
  formData.append('apiKey',apiKey);
  $.ajax({
      type: "POST",
      url: APP_URL+"/api/Reward/editReward",
      data: formData,
      contentType: false,
      cache: false,
      processData:false,
      success: function(data)
      {
        if(data.error == false){
          $('#rewardModal').modal('toggle');
          //save to audit
          var id = $('#rewardId').val();
        //   saveAudit(moduleName,id,'Update',apiKey);
          alertDialog(data.message);
          getDataTable();
        }else
          alert(data.message);
        $('.overlay').css({'display':'none'});
      },error: function(){
        alert('Problem occur. Please try again');
        $('.overlay').css({'display':'none'});
      }
  });
}

function getDataTable(){
  document.getElementById("loader").style.display = "block";
  document.getElementById("rewardTbl").style.display = "none";

  $.ajax({
        type: "GET",
        url: APP_URL+"/api/Reward/getRewards",
        data: $.param({'apiKey':apiKey}),
        success: function(data){
            document.getElementById("loader").style.display = "none";
            document.getElementById("rewardTbl").style.display = "inline-table";

            var returnedData = data;
            var data = data['data'];
            if(data.length < 1){
                $('#rewardTbl').dataTable().fnClearTable();
            }else{
                $('#rewardTbl').dataTable().fnClearTable();
                $('#rewardTbl').dataTable().fnAddData(data);
            }
        },error: function(jqXHR, exception){
          if(jqXHR.status == 500){
              document.getElementById("loader").style.display = "block";
             location.reload();
           }else{
            $('#rewardTbl').dataTable().fnClearTable();
            document.getElementById("loader").style.display = "none";
            document.getElementById("rewardTbl").style.display = "inline-table";
            alert('Error Occured');
           }
        }
    });
}

function checkFileSize(){
  if($("#image").val() != ''){
    var maxFileSize = parseInt('<?php echo $maxFileSize; ?>');
    var size = $('#image')[0].files[0].size;
    size = size/1000;
    if(size > maxFileSize){
      alert('File uploaded exceed maximum file size allowed')
      return 0;
    }
  }
  return 1;
}

</script>
@stop
