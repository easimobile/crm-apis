<?php

namespace App\Http\Controllers\Api;

use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\ApiController as ApiController;
use DB;
use Request, View;
use Illuminate\Support\Facades\Input;
use Response;
use Session;
use Validator;
use DateTime;
use Log;
use Config;

class EventApiController extends BaseController
{
	public function __construct()
    {
        $this->apiCtrl = new ApiController();
        // $this->log = new LogController();
    }


    public function getEvents(){
        $data = DB::table('Events')->orderby('eventId', 'desc')->get();

        if(count($data) > 0){
            foreach($data as $d){
                $startDate = $d->startDate;
                $endDate = $d->endDate;
                $d->startDate = date('Y-m-d H:i:s', strtotime($startDate));
                $d->endDate = date('Y-m-d H:i:s', strtotime($endDate));
            }

            return Response::json(array(
                'error' => false,
                'code' => 200,
                'message' => "Successfully retrieve events",
                'data'=> $data
            ));
        }else{
            return Response::json(array(
                'error' => false,
                'code' => 200,
                'message' => "Empty events",
                'data'=> $data
            ));
        }
    }

    public function getEventTypes(){
        $data = DB::table('types')->select('type')->where('category','EVENT')->get();

        if(count($data) > 0){
            return Response::json(array(
                'error' => false,
                'code' => 200,
                'message' => "Successfully retrieve event types",
                'data'=> $data
            ));
        }else{
            return Response::json(array(
                'error' => false,
                'code' => 200,
                'message' => "Empty event types",
                'data'=> $data
            ));
        }
    }

    public function editEvent(){
        $rules = array(
            'eventId'               => 'required',
            'eventName'             => 'required',
            'eventShortDescription' => 'required',
            'eventLongDescription'  => 'required',
            'eventTnc'              => 'required',
            'startDate'             => 'required',
            'endDate'               => 'required',
            'type'                  => 'required',
            'active'                => 'boolean'
        );

        $validator = Validator::make(Input::all(), $rules);
        if($validator->passes()){
            $eventId = Input::get('eventId');
            $eventName = Input::get('eventName');
            $eventShortDescription = Input::get('eventShortDescription');
            $eventLongDescription = Input::get('eventLongDescription');
            $eventTnc = Input::get('eventTnc');
            $startDate = Input::get('startDate');
            $endDate = Input::get('endDate');
            $type = Input::get('type');
            $startDate = date('Y-m-d H:i:s', strtotime($startDate));
            $endDate = date('Y-m-d H:i:s', strtotime($endDate));
            $active = Request::get('active');

            if(!$active)
                $active = 0;

            $updateArray = array(   "eventName"             => $eventName,
                                    "eventShortDescription" => $eventShortDescription,
                                    "eventLongDescription"  => $eventLongDescription,
                                    "eventTnc"              => $eventTnc,
                                    "startDate"             => $startDate,
                                    "endDate"               => $endDate,
                                    "type"                  => $type,
                                    "isActive"              => $active
                                );
            if(Input::hasFile('image')) {
                $rules = array(
                    'image' => 'required|image|mimes:jpg,jpeg'
                );

                $validator = Validator::make(Input::all(), $rules);
                if(!$validator->passes()){
                    DB::rollback();
                    $validationErrorString = implode(',',$validator->errors()->all());
                    $this->log->error($validationErrorString);
                    return Response::json(array(
                        'error' => true,
                        'code' => 400,
                        'message' => $validationErrorString
                    ));
                }

                $image = Input::file('image');
                $imagedata = file_get_contents($image);
                $imageInBase64 = base64_encode($imagedata);
                $updateArray['imageInBase64'] = $imageInBase64;
            }else{
                $existImage = DB::table('events')->where('eventId',$eventId)->value('imageInBase64');
                if(!$existImage){
                    return Response::json(array(
                        'error' => true,
                        'code' => 500,
                        'message' => 'Please insert an image for the event.'
                    ));
                }
            }

            DB::beginTransaction();
            try{
                DB::table('events')->where('eventId',$eventId)->update($updateArray);

                DB::commit();
                return Response::json(array(
                    'error' => false,
                    'code' => 200,
                    'message' => 'Event update successfully'
                ));
            }catch(Exception $e){
                DB::rollback();
                Log::error($e);
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => 'Error Occured(Edit Event)'
                ));
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }

    public function addEvent(){

        $rules = array(
            'eventName'             => 'required',
            'eventShortDescription' => 'required',
            'eventLongDescription'  => 'required',
            'eventTnc'              => 'required',
            'startDate'             => 'required',
            'endDate'               => 'required',
            'type'                  => 'required',
            'active'                => 'boolean'
        );

        $validator = Validator::make(Input::all(), $rules);
        if($validator->passes()){
            $eventName = Input::get('eventName');
            $eventShortDescription = Input::get('eventShortDescription');
            $eventLongDescription = Input::get('eventLongDescription');
            $eventTnc = Input::get('eventTnc');
            $startDate = Input::get('startDate');
            $endDate = Input::get('endDate');
            $type = Input::get('type');
            $startDate=date('Y-m-d H:i:s', strtotime($startDate));
            $endDate=date('Y-m-d H:i:s', strtotime($endDate));
            $active = Request::get('active');

            if(!$active)
                $active = 0;

            $insertArray = array(   "eventName"             => $eventName,
                                    "eventShortDescription" => $eventShortDescription,
                                    "eventLongDescription"  => $eventLongDescription,
                                    "eventTnc"              => $eventTnc,
                                    "startDate"             => $startDate,
                                    "endDate"               => $endDate,
                                    "type"                  => $type,
                                    "createdAt"             => date('Y-m-d H:i:s'),
                                    "isActive"               => $active
                                );

            if(Input::hasFile('image')) {
                $rules = array(
                    'image' => 'required|mimes:jpg,jpeg'
                );
                $validator = Validator::make(Input::all(), $rules);
                if($validator->passes()){
                    $image = Input::file('image');
                    $imagedata = file_get_contents($image);
                    $imageInBase64 = base64_encode($imagedata);
                    $insertArray['imageInBase64'] = $imageInBase64;
                }else{
                    DB::rollback();
                    return Response::json(array(
                        'error' => true,
                        'code' => 404,
                        'message' => json_encode($validator->errors()->all())
                    ));
                }
            }else{
                $copyCode = Input::get('copyCode');
                if($copyCode){
                    $file = DB::table('event')->where('eventId',$copyCode)->value('imageInBase64');
                    if($file){
                        //copy file
                        $insertArray['imageInBase64'] = $file;
                    }else{
                        DB::rollback();
                        return Response::json(array(
                            'error' => true,
                            'code' => 500,
                            'message' => "Unable to copy image. Please insert a new image."
                        ));
                    }
                }else{
                    return Response::json(array(
                        'error' => true,
                        'code' => 500,
                        'message' => "Please insert an image."
                    ));
                }
            }

            DB::beginTransaction();
            try{
                DB::table('events')->insert($insertArray);
                DB::commit();
                return Response::json(array(
                    'error' => false,
                    'code' => 200,
                    'message' => 'New events added successfully'
                ));
            }catch(Exception $e){
                DB::rollback();
                Log::error($e);
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => 'Error Occured(Add Event)'
                ));
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }

    public function deleteEvent(){
        $eventId = Request::get('eventId');

        if(!$eventId){
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => 'Event Id is required'
            ));
        }

        DB::beginTransaction();
        try{
            DB::table('events')->where('eventId',$eventId)->delete();
            DB::commit();
            return Response::json(array(
                'error' => false,
                'code' => 200,
                'message' => "'".$eventId."' has been deleted successfully"
            ));
        }catch(Exception $e){
            DB::rollback();
            Log::error($e);
            return Response::json(array(
                'error' => true,
                'code' => 500,
                'message' => 'Error Occured(Delete event)'
            ));
        }
    }
}
