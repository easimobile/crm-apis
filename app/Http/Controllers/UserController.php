<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use DB;
use Request, View;
use GuzzleHttp\Client;
use Session;
use Illuminate\Support\Facades\Input;
use Validator;
use Redirect;
use DateTime;
use Config;
use Response;
use App\Http\Controllers\general\globalController as globalController;

class UserController extends BaseController
{
	public function __construct()
    {
        $this->globalCtrl = new globalController();
    }

	public function retrieveSysFlagSetting($flag){
        //for internal api use
		$setting = DB::table('sysflag')->where('flag',$flag)->value('setting');
		return $setting;
	}

	public function getData($api, $param){
		$client = new Client();
        $res = $client->request('GET', $api."?".http_build_query($param));
        $data = $res->getBody();
        $data = json_decode($data);

        if($data->error == false){
            $data = $data->data;
            $data = (array)$data;
            return $data;
        }else{
            return array();
        }
	}

	public function postData($api,$data){

		$ch = curl_init($api);
  		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
  		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  		curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
  		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
  		$result = curl_exec($ch);
  		curl_close($ch);
  		$result = json_decode($result);

  		return $result;
	}

    /* public function saveAudit($param){
        $auditApi = asset('api/saveAuditLog');
        $audit = $this->postData($auditApi,$param);
        return $audit;
    } */

    public function login(){

    	if(Input::all()){
    		$rules = array(
        		'id'          => 'required',
        		'password'    => 'required',
        	);

        	$validator = Validator::make(Input::all(), $rules);
        	if($validator->passes()){

        		$data = array(
					'id'=>Input::get('id'),
					'password'=>Input::get('password')
				);
    			$api = asset('api/login');
    			$login = $this->postData($api,$data);


    			if($login->error == true){
                        if($login->code == 401){
                            return Redirect::to('changeExpiredPassword')->with('userCode', Input::get('id'));
                        }else if($login->code == 202){
                            return Redirect::to('resetPassword')->with('userCode',Input::get('id'))->with('password',Input::get('password'));
                        }else
    					   return Redirect::back()->with('fail',$login->message)->withInput(Request::except('password'));
    			}
    			else{
                    $apiKey = $login->apiKey;
                    Session::put('apiKey',$apiKey);

                    $moduleName = Config::get('moduleName.web');
                    $recordNo = '';
                    $actionName = 'Login';

                    //insert into audit
                    /* $param = array('moduleName'=>$moduleName,'recordNo'=>$recordNo,'actionName'=>$actionName,'apiKey'=>$apiKey);
                    $audit = $this->saveAudit($param); */

					$userCode = Input::get('id');

					$empcd=DB::table('employee')->where('emp_cd',$userCode)->orwhere('emp_id',$userCode)->value('emp_cd');

                    //check
                    $check = DB::table('EGSecurityWeb')
                    ->join('employee','employee.emp_grp','=','EGSecurityWeb.UGroupCode')
                    ->join('EPROGRAMWeb','EPROGRAMWeb.ProgramName','=','EGSecurityWeb.ProgramName')
					->Where('employee.emp_cd',$empcd)
                    ->where('EGSecurityWeb.AllowYesNo',1)
                    ->where('EPROGRAMWeb.ParentProgramName',NULL)
                    ->where('EPROGRAMWeb.Active',1)
                    ->orderBy('EPROGRAMWeb.MenuSequence','asc')
                    ->select('EGSecurityWeb.ProgramName')
                    ->first();

                    if($check){

                         $checkIfHasChild = DB::table('EPROGRAMWeb')
                            ->join('EGSecurityWeb','EPROGRAMWeb.ProgramName','=','EGSecurityWeb.ProgramName')
                            ->join('employee','employee.emp_grp','=','EGSecurityWeb.UGroupCode')
                            ->where('EPROGRAMWeb.Active',1)
                            ->where('employee.emp_cd',$empcd)
                            ->where('ParentProgramName',$check->ProgramName)
                            ->where('AllowYesNo',1)
                            ->orderBy('EPROGRAMWeb.MenuSequence','asc')
                            ->select('EGSecurityWeb.ProgramName')
                            ->first();

                        if($checkIfHasChild){
                            $checkIfHasSecondChild = DB::table('EPROGRAMWeb')
                            ->join('EGSecurityWeb','EPROGRAMWeb.ProgramName','=','EGSecurityWeb.ProgramName')
                            ->join('EGROUP','EGROUP.UGroupCode','=','EGSecurityWeb.UGroupCode')
                            ->where('EPROGRAMWeb.Active',1)
                            ->where('EGROUP.UserCode',$empcd)
                            ->where('ParentProgramName',$checkIfHasChild->ProgramName)
                            ->where('AllowYesNo',1)
                            ->orderBy('EPROGRAMWeb.MenuSequence','asc')
                            ->select('EGSecurityWeb.ProgramName')
                            ->first();

                            if($checkIfHasSecondChild){
                                $route = $check->ProgramName.'/'.$checkIfHasChild->ProgramName.'/'.$checkIfHasSecondChild->ProgramName;
                                if($login->code == 302)
                                    return redirect($route)->with('message',$login->message);
                                else
                                    return redirect($route);
                            }else{
                                //$route = $checkIfHasChild->ProgramName;
                                $route = $check->ProgramName.'/'.$checkIfHasChild->ProgramName;
                                if($login->code == 302)
                                    return redirect($route)->with('message',$login->message);
                                else
                                    return redirect($route);
                            }
                        }else{
                            $route = $check->ProgramName;
                            if($login->code == 302)
                                return redirect($route)->with('message',$login->message);
                            else
                                return redirect($route);
                        }
                    }else{
                        if($login->code == 302)
                            return Redirect('contactUs')->with('message',$login->message);
                        else
                            return Redirect('contactUs');
                    }
    			}
        	}
        	else{
        		return Redirect::back()->withErrors($validator)->withInput(Request::except('password'));
        	}

    	}
    	else{
            $tabTitle = getenv('TAB_TITLE');
        	return view::make('login',compact('tabTitle'));
        }
    }

    public function logout(){
        $sessionFlag = Request::get('sessionFlag');

        // $apiKey = $this->globalCtrl->getApiKey();
        // $moduleName = Config::get('moduleName.web');
        // $recordNo = '';
        // $actionName = 'Logout';

        //insert into audit
        /* $param = array('moduleName'=>$moduleName,'recordNo'=>$recordNo,'actionName'=>$actionName,'apiKey'=>$apiKey);
        $audit = $this->saveAudit($param); */

    	Session::flush();
        if($sessionFlag == 1){
            Session::flash('fail','Session Timeout');
        }else
    	   return redirect('login')->with('success',"Log Out Successfully");
    }


    public function contactUs(){
    	return view::make('contactUs');
    }

    public function changePw(){

    	if(Input::all()){

			 $rules = array(
        		'currentPassword'          => 'required',
        		'newPassword'              => 'required|confirmed',
        		'newPassword_confirmation' => 'required'
        	);

			$validator = Validator::make(Input::all(), $rules);

			if ($validator->passes()) {
				$currentPw = Input::get('currentPassword');
    			$newPw = Input::get('newPassword');
    			$newPw_confirmation = Input::get('newPassword_confirmation');
    			$userCode = Request::get('userCode');
                $apiKey = Request::get('apiKey');

                $data = array(
                    'currentPw'=>$currentPw,
                    'newPw'=>$newPw,
                    'newPw_confirmation'=> $newPw_confirmation
                );

                if(!$userCode){
                    $data['apiKey'] = $apiKey;
                }else
                    $data['userId'] = $userCode;

    			$api = asset('api/changePassword');
    			$post = $this->postData($api,$data);

    			if($post->error == true){
    				return Redirect::back()->withErrors($post->message);
    			}
    			else{
                    $moduleName = Config::get('moduleName.web');
                    $recordNo = '';
                    $actionName = 'Change Password';

                    //insert into audit
                    /* $param = array('moduleName'=>$moduleName,'recordNo'=>$recordNo,'actionName'=>$actionName,'apiKey'=>$apiKey,'recordNoUserCodeFlag'=>1);
                    $audit = $this->saveAudit($param); */

    				return Redirect::back()->with('success',$post->message);
    			}
			}
			else{
				return Redirect::back()->withErrors($validator)->withInput();
			}
    	}else{
            $apiKey = $this->globalCtrl->getApiKey();
    		return view::make('changePassword',compact('apiKey'));
    	}
    }

    public function resetPassword(){
        if(Input::all()){
             $rules = array(
                'newPassword'              => 'required|confirmed',
                'newPassword_confirmation' => 'required'
            );

            $userId = Input::get('userCode');

            //change to an api
            $checkPasswordFirst = DB::table('employee')->where('emp_cd',$userId)->value('pwd_first');
            if(!$checkPasswordFirst){
                return Redirect::to('login')->with('alert',"Password already changed.");
            }

            $validator = Validator::make(Input::all(), $rules);

            if ($validator->passes()) {
                $newPw = Input::get('newPassword');
                $newPw_confirmation = Input::get('newPassword_confirmation');
                $userId = Input::get('userCode');

                $data = array(
                    'newPw'=>$newPw,
                    'newPw_confirmation'=> $newPw_confirmation,
                    'userId' => $userId
                );

                $api = asset('api/resetPassword');
                $post = $this->postData($api,$data);

                if($post->error == true){
                    return Redirect::back()->withErrors($post->message)->withInput();
                }
                else{
                    $moduleName = Config::get('moduleName.web');
                    $recordNo = '';
                    $actionName = 'Reset Password';

                    //insert into audit
                    /* $param = array('moduleName'=>$moduleName,'recordNo'=>$recordNo,'actionName'=>$actionName,'userCode'=>$userId,'recordNoUserCodeFlag'=>1);
                    $audit = $this->saveAudit($param); */

                    return Redirect::to('login')->with('success',$post->message);
                }
            }else{
                return Redirect::back()->withErrors($validator)->withInput();
            }
        }else{
            $tabTitle = getenv('TAB_TITLE');
             return view::make('resetPassword',compact('tabTitle'));
        }
    }

    public function changeExpiredPassword(){

        if(Input::all()){
            //if(!(Input::get('userCode'))){}
             $rules = array(
                'currentPassword'          => 'required',
                'newPassword'              => 'required|confirmed',
                'newPassword_confirmation' => 'required'
            );

            $validator = Validator::make(Input::all(), $rules);

            if ($validator->passes()) {
                $currentPw = Input::get('currentPassword');
                $newPw = Input::get('newPassword');
                $newPw_confirmation = Input::get('newPassword_confirmation');
                $userId = Input::get('userCode');

                $data = array(
                    'currentPw'=>$currentPw,
                    'newPw'=>$newPw,
                    'newPw_confirmation'=> $newPw_confirmation,
                    'userId' => $userId
                );

                $api = asset('api/changePassword');
                $post = $this->postData($api,$data);

                if($post->error == true){
                    return Redirect::back()->with('userCode',$userId)->withErrors($post->message);
                }
                else{
                    $moduleName = Config::get('moduleName.web');
                    $recordNo = '';
                    $actionName = 'Change Password';

                    //insert into audit
                    /* $param = array('moduleName'=>$moduleName,'recordNo'=>$recordNo,'actionName'=>$actionName,'userCode'=>$userId,'recordNoUserCodeFlag'=>1);
                    $audit = $this->saveAudit($param); */

                    return Redirect::to('login')->with('success',$post->message);
                }
            }else{
                return Redirect::back()->with('userCode',$userId)->withErrors($validator);
            }

        }else{
            $tabTitle = getenv('TAB_TITLE');
            return view::make('changeExpiredPassword',compact('tabTitle'));
        }
    }

    public function utf8_encode_deep(&$input) {
        if (is_string($input)) {
            $input = utf8_encode($input);
        } else if (is_array($input)) {
            foreach ($input as &$value) {
                self::utf8_encode_deep($value);
            }

            unset($value);
        } else if (is_object($input)) {
            $vars = array_keys(get_object_vars($input));

            foreach ($vars as $var) {
                self::utf8_encode_deep($input->$var);
            }
        }
    }

    public function error(){
        return view::make('errors.401');
    }
}
