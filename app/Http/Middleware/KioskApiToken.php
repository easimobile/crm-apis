<?php

namespace App\Http\Middleware;

use Closure;
use Response;
use Request;
use DB;
use Route;
use Exception;
use DateTime;
use App\Http\Controllers\LogController as LogController;
use App\Http\Controllers\Api\GeneralApiController as GeneralApiController;
use App\Http\Controllers\Api\v2\MobilePurchase\MobilePurchaseApiController as MobilePurchaseApiController;

class KioskApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next){
         if(Request::get('token') == '1234567'){
            $ipAddress = '192.168.8.158';
            $request->attributes->add(['machineId' => $ipAddress]);
            return $next($request);
        }
		
        $key = DB::table('sysflag')->where('flag','WC3')->value('setting');

        $ctrl = new GeneralApiController();

        $token = Request::get('token');
        $routeName = Route::getCurrentRoute()->getPath();
        $routeName = basename($routeName);
        $destination = asset('/').Route::getCurrentRoute()->getPath();

        if(!$token){
            $message = "Token is required";
            $this->_insertEventLogger('Kiosk', $routeName.'Request',json_encode(Request::all()),$destination);  
            $this->_insertEventLogger('Kiosk',$routeName.'Response',$message,$destination);
            return Response::json(array(
                'error' => true,
                'code' => '404',
                'message' => $message
            ));
        }else{
            //decrypt
            for($x=0;$x <=2;$x++){
                $this->secretKey = $ctrl->_decrypt($key);
                $data = $this->decrypt($token,$this->secretKey);
                $data = explode('|',$data);
                if(count($data) == 2){
                    $ipAddress = $data[0];
                    $datetime = $data[1];
					$datetime=substr_replace($datetime ,"",-4);

                    try{
                        $datetime = strtotime($datetime);
                        $datetime = date('Y-m-d H:i:s',$datetime);
						
                    }catch(Exception $e){
                        $this->_insertEventLogger('Kiosk', $routeName.'Request',json_encode(Request::all()),$destination);  
                        $this->_insertEventLogger('Kiosk',$routeName.'Response',$e->getMessage." - ".json_encode($data),$destination);
                        return Response::json(array(
                            'error' => true,
                            'code'  => '500',
                            'message' => $e->getMessage
                        ));
                    }
					
                    //1. check time - 5mins
                    $now = date('Y-m-d H:i:s');
                    $graceTime = date('Y-m-d H:i:s', strtotime($datetime."+5 min"));
					 
					
                    if($graceTime >= $now){
                        //2. check ip
 						if(strpos($_SERVER['REQUEST_URI'], 'submitOrderSummary') !== false)
						{
							$check = DB::table('sysflag')->where('flag','WQ0')->value('setting');
							if($check!==$ipAddress)
								{
									return Response::json(array(
										'error' => true,
										'code'  => '401',//'500',
										'message' => 'wrong IP address'
									)); 
								}
						
						}else{
					
                        $check = DB::table('mas_pos')->where('ipAddress',$ipAddress)->first(); 
						
						}
						
                        if($check){
                            //pass mid to controller
                            $request->attributes->add(['machineId' => $ipAddress]);
                            return $next($request);
                        }else{
                            $message = "Invalid Token";
                            $this->_insertEventLogger('Kiosk', $routeName.'Request',json_encode(Request::all()),$destination);  
                            $this->_insertEventLogger('Kiosk',$routeName.'Response',$message." (line 88)"." - ".json_encode($data),$destination);
                            return Response::json(array(
                                'error' => true,
                                'code'  => '500',//'500',
                                'message' => $message
                            )); 
                        }
                    }else{
                        $message = "Token Expired";
                        $this->_insertEventLogger('Kiosk', $routeName.'Request',json_encode(Request::all()),$destination);  
                        $this->_insertEventLogger('Kiosk',$routeName.'Response',$message." - ".json_encode($data),$destination);
                        return Response::json(array(
                            'error' => true,
                            'code'  => '500',
                            'message' => $message
                        ));
                    }
                }else{
                    if($x == 2){
                        $message = "Invalid Token";
                        $this->_insertEventLogger('Kiosk', $routeName.'Request',json_encode(Request::all()),$destination);  
                        $this->_insertEventLogger('Kiosk',$routeName.'Response',$message." (line 108)"." - ".json_encode($data),$destination);
                        return Response::json(array(
                            'error' => true,
                            'code'  => '401',//'500',
                            'message' => $message
                        )); 
                    }
                }
            }
            //end
        }
    }

    private function _insertEventLogger($eventLoggerType, $eventLoggerFunction, $eventLoggerLog,$eventLoggerDestination){
        try{
            $eventLogArray = array('EventLoggerType'=>$eventLoggerType,'EventLoggerFunction'=>$eventLoggerFunction,'EventLoggerLog'=>$eventLoggerLog,'EventLoggerTime'=>date('Y-m-d H:i:s'),'EventLoggerDestination'=>$eventLoggerDestination);

            DB::table('eventLogger')->insert($eventLogArray);

        }catch(Exception $e){
            $this->log = new LogController();
            $this->log->error($e->getMessage());
        }
    }

    private function encrypt($token, $secretKey){
        $ctrl = new GeneralApiController();
        //$string = str_replace(" ","+",$string);
        $key = $ctrl->md5hash($secretKey);
        $iv = "";

        $encrypted = openssl_encrypt($token, 'des-ede3', $key, 0, $iv);
        return $encrypted;
    }

    private function decrypt($string, $secretKey){
        
        $ctrl = new GeneralApiController();
        $string = str_replace(" ","+",$string);
        $string = str_replace("\\","",$string);
        $key = $ctrl->md5hash($secretKey);
        $iv = "";
        $decrypted = openssl_decrypt($string, 'des-ede3', $key, 0, $iv);

        return $decrypted;
    }
}