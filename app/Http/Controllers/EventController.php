<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use DB;
use Request, View;
use GuzzleHttp\Client;
use Session;
use Illuminate\Support\Facades\Input;
use Validator;
use Redirect;
use Config;
use App\Http\Controllers\general\globalController as globalController;

class EventController extends BaseController
{
	public function __construct()
    {
    	$this->userCtrl = new UserController();
    	$this->globalCtrl = new globalController();
    }

	public function event(){

    	$apiKey = $this->globalCtrl->getApiKey();
        $param = array('apiKey'=>$apiKey);

        $param = array('apiKey'=>$apiKey);
        $eventTypeApi = asset("api/Event/getEventTypes");
        $eventTypes = $this->userCtrl->getData($eventTypeApi, $param);

        $fileSizeParam = array('sysflag'=>'WA1');
        $sysflagApi = asset("api/getSysflagValue");
        $data = $this->userCtrl->getData($sysflagApi,$fileSizeParam);
        $maxFileSize = $data['setting'];

        //audit
        /* $moduleName = Config::get('moduleName.event');
        $recordNo = '';
        $actionName = 'Open'; */

        //insert into audit
        /* $param = array('moduleName'=>$moduleName,'recordNo'=>$recordNo,'actionName'=>$actionName,'apiKey'=>$apiKey);
        $audit = $this->userCtrl->saveAudit($param); */

    	return view::make('configuration/event/event',compact('apiKey','eventTypes','maxFileSize'));
    }
}
