<?php

namespace App\Console\Commands;
use DB;
use Log;
use App\Http\Controllers\ApiController as ApiController;
use App\Http\Controllers\Api\MobileApiController as MobileApiController;
use Illuminate\Console\Command;

class CheckPointExpiryDataService extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetch:CheckPointExpiry';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check point expiry';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->apiCtrl = new ApiController();
        $this->mobileApiCtrl = new MobileApiController();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::beginTransaction();
        try{
            $data = DB::table('Point')
                    ->select('uid', DB::RAW('SUM(point) as points'))
                    ->where('type', 'POINT_ISSUANCE')
                    ->where('isExpired', 0)
                    ->where('insertDate', '<=', date('Y-m-d', strtotime('-30 days')))
                    ->groupby('uid')
                    ->get();

            DB::table('Point')
                ->where('type', 'POINT_ISSUANCE')
                ->where('isExpired', 0)
                ->where('insertDate', '<=', date('Y-m-d', strtotime('-30 days')))
                ->update(array('isExpired' => 1));

            foreach($data as $d){
                $customerDetails = DB::table('customer')->select('usablePoints', 'availablePoints')->where('cust_no', $d->uid)->first();
                $usablePoints = $customerDetails->usablePoints;
                $availablePoints = $customerDetails->availablePoints;
                $usablePoints -= $d->points;
                $availablePoints -= $d->points;
                DB::table('customer')->where('Cust_no',$d->uid)->update(array('usablePoints' => $usablePoints, 'availablePoints' => $availablePoints));

                $messageContent = DB::table('notificationMessage')->where('category','POINT_EXPIRY')->first();
                $title = $messageContent->title;
                $body = $messageContent->body;
                if(strpos($body, '[POINT]') !== false) {
                    $body = str_replace("[POINT]",$d->points,$body);
                }

                $this->mobileApiCtrl->_sendNotification($title,$body,'POINT_EXPIRY',$d->uid);
            }
            DB::commit();
            $msg = 'Expiry point checked successfully';
            Log::info($msg);
            $this->info($msg);
        }catch(Exception $e){
            DB::rollback();
            $this->apiCtrl->_insertEventLogger('Scheduler', 'CheckPointExpiry' , json_encode($e->getMessage()), 'CheckPointExpiryDataService');
            Log::error($e->getMessage());
            $this->info($e->getMessage());
        }
    }
}
