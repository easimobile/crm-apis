<?php 
namespace App\Http\Composers;
use Illuminate\Contracts\View\View;
use DB;
use Session;
use Route;
use Config;
use App\Http\Controllers\ApiController as apiController;

class MasterComposer {

    public function compose(View $view)
    {
        $this->apiCtrl = new apiController;
    	//get program
        $tabTitle = Config::get('backend.tabTitle');
        $masterTitleMini = Config::get('backend.masterTitleMini');
        $masterTitle1 = Config::get('backend.masterTitle1');
        $masterTitle2 = Config::get('backend.masterTitle2');
        $idleTime = Config::get('backend.idleTime');
        $idleTime = $idleTime*60*1000;

        $apiKey = Session::get('apiKey');
        $userCode = DB::table('apiAuth')->where('apikey',$apiKey)->value('userId');

        $userInfo = $this->apiCtrl->_getUserInfo($apiKey);
		$routeName = Route::currentRouteName();

/*    	$groupCode = DB::table('EGROUP')->join('EMPLOYEE','EGROUP.UserCode','=','EMPLOYEE.EMP_CD')
    	->where('EMPLOYEE.EMP_CD',$userCode)->value('UGroupCode');*/
        
        $groupCode = DB::table('employee')->where('emp_cd',$userCode)->value('emp_grp');

		$menus = DB::table('EPROGRAMWeb')
				->join('EGSecurityWeb','EPROGRAMWeb.ProgramName','=','EGSecurityWeb.ProgramName')
				->where('EGSecurityWeb.UGroupCode',$groupCode)
				->where('AllowYesNo',1)
				->where('Active',1)
				->where('ParentProgramName',NULL)
				->orderBy('MenuSequence','asc')
				->get();

        $menu = array();

        foreach($menus as $menu2){
            $subMenu = DB::table('EPROGRAMWeb')
                ->join('EGSecurityWeb','EPROGRAMWeb.ProgramName','=','EGSecurityWeb.ProgramName')
                ->where('EGSecurityWeb.UGroupCode',$groupCode)
                ->where('AllowYesNo',1)
                ->where('Active',1)
                ->where('ParentProgramName',$menu2->ProgramName)
                ->orderBy('MenuSequence','asc')
                ->get();

            foreach($subMenu as $menu3){
                $subMenu2 = DB::table('EPROGRAMWeb')
                    ->join('EGSecurityWeb','EPROGRAMWeb.ProgramName','=','EGSecurityWeb.ProgramName')
                    ->where('EGSecurityWeb.UGroupCode',$groupCode)
                    ->where('AllowYesNo',1)
                    ->where('Active',1)
                    ->where('ParentProgramName',$menu3->ProgramName)
                    ->orderBy('MenuSequence','asc')
                    ->get();

                    $menu3->subProgram = $subMenu2;
            }

            $menu2->subProgram = $subMenu;

            array_push($menu,$menu2);
        }

		//get user authorized feature
        $userFeatureFlag = DB::table('EGSecurityWeb')
        	->join('EGROUP','EGROUP.UGroupCode','=','EGSecurityWeb.UGroupCode')
            ->where('EGROUP.UserCode',$userCode)
            ->where('ProgramName',$routeName)
            ->first();

        $view->with(compact('menu','userInfo','userFeatureFlag','tabTitle','masterTitleMini','masterTitle1','masterTitle2','idleTime'));
    }

}