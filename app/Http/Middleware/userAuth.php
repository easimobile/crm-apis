<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use DB;
use Route;
use Response;

class userAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Session::has('apiKey')){
            return redirect('login');
        }
        else{
            $apiKey = Session::get('apiKey');

            $userCode = DB::table('apiAuth')->where('apiKey',$apiKey)->value('userId');

            if($userCode){
                $latestKey = DB::table('apiAuth')->where('userId',$userCode)->latest('createdAt')->value('apiKey');

                if($latestKey != $apiKey){
                    Session::flush();
                    return redirect('login')->with('fail',"You have been logged out because you've signed in on another device");
                }
            }else{
                Session::flush();
                return redirect('login')->with('fail','Error Occured');
            }

            $routeName = Route::currentRouteName();
            $registeredProgram = DB::table('EPROGRAMWeb')->where('ProgramName',$routeName)->where('active',1)->first();

            if($registeredProgram)
            {
                //check parent
                $checkParent = DB::table('EPROGRAMWeb')->where('ProgramName',$routeName)->value('parentProgramName');

                if($checkParent){
                    $checkParentAuth = DB::table('EGSecurityWeb')
                    ->join('employee','employee.emp_grp','=','EGSecurityWeb.UGroupCode')
                    ->where('employee.emp_cd',$userCode)
                    ->where('ProgramName',$checkParent)
                    ->value('AllowYesNo');

                    if($checkParentAuth)
                    {
                        $check = DB::table('EGSecurityWeb')
                        ->join('employee','employee.emp_grp','=','EGSecurityWeb.UGroupCode')
                        ->where('employee.emp_cd',$userCode)
                        ->where('ProgramName',$routeName)
                        ->value('AllowYesNo');

                        if($check)
                            return $next($request);
                        else{
                            return redirect('error_401');
                        }
                    }else{
                        return redirect('error_401');
                    }
                }
                else{
                    $check = DB::table('EGSecurityWeb')
                    ->join('employee','employee.emp_grp','=','EGSecurityWeb.UGroupCode')
                    ->where('employee.emp_cd',$userCode)
                    ->where('ProgramName',$routeName)
                    ->value('AllowYesNo');

                    if($check)
                        return $next($request);
                    else
                        return redirect('error_401');
                }
            }
            else{
                //return $next($request);
                $routeName = Route::getCurrentRoute()->getPath();
                if($routeName == 'changePassword' || $routeName == 'contactUs')
                    return $next($request);
                else
                    return Response::view('errors.404', [], 404);
            }
        }
    }
}
