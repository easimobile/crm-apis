<?php

namespace App\Http\Controllers\Api;

use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\ApiController as ApiController;
use DB;
use Request, View;
use Illuminate\Support\Facades\Input;
use Response;
use Session;
use Validator;
use DateTime;
use Log;
use Config;

class RewardApiController extends BaseController
{
	public function __construct()
    {
        $this->apiCtrl = new ApiController();
        // $this->log = new LogController();
    }


    public function getRewards(){
        $data = DB::table('reward')->orderby('rewardId', 'desc')->get();

        if(count($data) > 0){
            foreach($data as $d){
                $startDate = $d->startDate;
                $endDate = $d->endDate;
                $d->startDate = date('Y-m-d H:i:s', strtotime($startDate));
                $d->endDate = date('Y-m-d H:i:s', strtotime($endDate));
            }

            return Response::json(array(
                'error' => false,
                'code' => 200,
                'message' => "Successfully retrieve rewards",
                'data'=> $data
            ));
        }else{
            return Response::json(array(
                'error' => false,
                'code' => 200,
                'message' => "Empty reward",
                'data'=> $data
            ));
        }
    }

    public function editReward(){
        $rules = array(
            'rewardId'                  => 'required',
            'rewardName'                => 'required',
            'rewardShortDescription'    => 'required',
            'rewardLongDescription'     => 'required',
            'rewardTnc'                 => 'required',
            'startDate'                 => 'required',
            'endDate'                   => 'required',
            'maxRedeemedPerReward'      => 'required',
            'maxRedeemedPerUser'        => 'required',
            'type'                      => 'required',
            'pointsToRedeem'            => 'required',
            'active'                    => 'boolean'
        );

        $validator = Validator::make(Input::all(), $rules);
        if($validator->passes()){
            $rewardId = Input::get('rewardId');
            $rewardName = Input::get('rewardName');
            $rewardShortDescription = Input::get('rewardShortDescription');
            $rewardLongDescription = Input::get('rewardLongDescription');
            $rewardTnc = Input::get('rewardTnc');
            $startDate = Input::get('startDate');
            $endDate = Input::get('endDate');
            $maxRedeemedPerReward = Input::get('maxRedeemedPerReward');
            $maxRedeemedPerUser = Input::get('maxRedeemedPerUser');
            $type = Input::get('type');
            $location = Input::get('location');
            $pointsToRedeem = Input::get('pointsToRedeem');
            $startDate = date('Y-m-d H:i:s', strtotime($startDate));
            $endDate = date('Y-m-d H:i:s', strtotime($endDate));
            $active = Request::get('active');

            if(!$active)
                $active = 0;

            $updateArray = array(   "rewardName"                => $rewardName,
                                    "rewardShortDescription"    => $rewardShortDescription,
                                    "rewardLongDescription"     => $rewardLongDescription,
                                    "rewardTnc"                 => $rewardTnc,
                                    "startDate"                 => $startDate,
                                    "endDate"                   => $endDate,
                                    "type"                      => $type,
                                    "maxRedeemedPerReward"      => $maxRedeemedPerReward,
                                    "maxRedeemedPerUser"        => $maxRedeemedPerUser,
                                    "pointsToRedeem"            => $pointsToRedeem,
                                    "location"                  => $location,
                                    "isActive"                  => $active
                                );

            if(Input::hasFile('image')) {
                $rules = array(
                    'image' => 'required|image|mimes:jpg,jpeg'
                );

                $validator = Validator::make(Input::all(), $rules);
                if(!$validator->passes()){
                    DB::rollback();
                    $validationErrorString = implode(',',$validator->errors()->all());
                    return Response::json(array(
                        'error' => true,
                        'code' => 400,
                        'message' => $validationErrorString
                    ));
                }

                $image = Input::file('image');
                $imagedata = file_get_contents($image);
                $imageInBase64 = base64_encode($imagedata);
                $updateArray['imageInBase64'] = $imageInBase64;
            }else{
                $existImage = DB::table('Reward')->where('rewardId',$rewardId)->value('imageInBase64');
                if(!$existImage){
                    return Response::json(array(
                        'error' => true,
                        'code' => 500,
                        'message' => 'Please insert an image for the reward.'
                    ));
                }
            }

            DB::beginTransaction();
            try{
                DB::table('Reward')->where('rewardId',$rewardId)->update($updateArray);

                DB::commit();
                return Response::json(array(
                    'error' => false,
                    'code' => 200,
                    'message' => 'Reward update successfully'
                ));
            }catch(Exception $e){
                DB::rollback();
                Log::error($e);
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => 'Error Occured(Edit Reward) '.$e
                ));
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }

    public function addReward(){

        $rules = array(
            'rewardName'                => 'required',
            'rewardShortDescription'    => 'required',
            'rewardLongDescription'     => 'required',
            'rewardTnc'                 => 'required',
            'startDate'                 => 'required',
            'endDate'                   => 'required',
            'maxRedeemedPerReward'      => 'required',
            'maxRedeemedPerUser'        => 'required',
            'type'                      => 'required',
            'pointsToRedeem'            => 'required',
            'active'                    => 'boolean'
        );

        $validator = Validator::make(Input::all(), $rules);
        if($validator->passes()){
            $rewardName = Input::get('rewardName');
            $rewardShortDescription = Input::get('rewardShortDescription');
            $rewardLongDescription = Input::get('rewardLongDescription');
            $rewardTnc = Input::get('rewardTnc');
            $startDate = Input::get('startDate');
            $endDate = Input::get('endDate');
            $maxRedeemedPerReward = Input::get('maxRedeemedPerReward');
            $maxRedeemedPerUser = Input::get('maxRedeemedPerUser');
            $type = Input::get('type');
            $location = Input::get('location');
            $pointsToRedeem = Input::get('pointsToRedeem');
            $startDate = date('Y-m-d H:i:s', strtotime($startDate));
            $endDate = date('Y-m-d H:i:s', strtotime($endDate));
            $active = Request::get('active');

            if(!$active)
                $active = 0;

            $insertArray = array(   "rewardName"             => $rewardName,
                                    "rewardShortDescription" => $rewardShortDescription,
                                    "rewardLongDescription"  => $rewardLongDescription,
                                    "rewardTnc"              => $rewardTnc,
                                    "startDate"              => $startDate,
                                    "endDate"                => $endDate,
                                    "maxRedeemedPerReward"   => $maxRedeemedPerReward,
                                    "maxRedeemedPerUser"     => $maxRedeemedPerUser,
                                    "type"                   => $type,
                                    "location"               => $location,
                                    "pointsToRedeem"         => $pointsToRedeem,
                                    "createdAt"              => date('Y-m-d H:i:s'),
                                    "isActive"               => $active
                                );

            if(Input::hasFile('image')) {
                $rules = array(
                    'image' => 'required|mimes:jpg,jpeg'
                );
                $validator = Validator::make(Input::all(), $rules);
                if($validator->passes()){
                    $image = Input::file('image');
                    $imagedata = file_get_contents($image);
                    $imageInBase64 = base64_encode($imagedata);
                    $insertArray['imageInBase64'] = $imageInBase64;
                }else{
                    DB::rollback();
                    return Response::json(array(
                        'error' => true,
                        'code' => 404,
                        'message' => json_encode($validator->errors()->all())
                    ));
                }
            }else{
                $copyCode = Input::get('copyCode');
                if($copyCode){
                    $file = DB::table('reward')->where('rewardId',$copyCode)->value('imageInBase64');
                    if($file){
                        //copy file
                        $insertArray['imageInBase64'] = $file;
                    }else{
                        DB::rollback();
                        return Response::json(array(
                            'error' => true,
                            'code' => 500,
                            'message' => "Unable to copy image. Please insert a new image."
                        ));
                    }
                }else{
                    return Response::json(array(
                        'error' => true,
                        'code' => 500,
                        'message' => "Please insert an image."
                    ));
                }
            }

            DB::beginTransaction();
            try{
                DB::table('Reward')->insert($insertArray);

                DB::commit();
                return Response::json(array(
                    'error' => false,
                    'code' => 200,
                    'message' => 'New reward added successfully'
                ));
            }catch(Exception $e){
                DB::rollback();
                Log::error($e);
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => 'Error Occured(Add Reward)'
                ));
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }

    /* public function rewardActivation(){
        $rules = array(
            'rewardId'            => 'rewardId',
            'active'              => 'boolean'
        );
        $validator = Validator::make(Request::all(), $rules);
        if($validator->passes()){

            $rewardId = Request::get('rewardId');
            $active = Request::get('active');

            if(!$active)
                $active = 0;

            if(!$rewardId){
                return Response::json(array(
                    'error' => true,
                    'code' => 400,
                    'message' => 'Reward Id is required'
                ));
            }

            DB::beginTransaction();
            try{
                DB::table('Reward')->where('rewardId',$rewardId)->update(array("isActive" => $active));
                DB::commit();
                if($active == 1){
                    return Response::json(array(
                        'error' => false,
                        'code' => 200,
                        'message' => "'".$rewardId."' has been activate successfully"
                    ));
                }else{
                    return Response::json(array(
                        'error' => false,
                        'code' => 200,
                        'message' => "'".$rewardId."' has been inactivate successfully"
                    ));
                }
            }catch(Exception $e){
                DB::rollback();
                Log::error($e);
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => 'Error Occured(rewardActivation) '.$e
                ));
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    } */
}
