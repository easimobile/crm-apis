@extends('layouts.master')
@section('content')
<style>
#saveEditedEventBtn,#addEventBtn, #closeModal{
  float: right;
  display: inline-block;
  margin: 1px;
}

.editEventButton, .viewEventButton, .copyEventButton, .deleteEventButton{
  padding: 2px 7px;
  margin: 1px;
}

.addBtn-Event{
  width: 100%;
  text-align: right;
  padding: 5px;
   display: inline-block;
}
</style>

<div class="overlay">
	<div id="loading-img"></div>
</div>
<div class="box-header">
    <h3 class="page-header">Events</h3>
</div>
@if($userFeatureFlag->AllowAdd == 1)
<div class="addBtn-Event" id="openAddEventDiv">
  <button type="button" class="btn btn-primary" id="openAddEventBtn"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
</div>
@endif
<div class="box-body" id="boxBody">
    <div id="loader" style="display:none;"></div>
    <table id="eventTbl" class="table table-bordered table-hover">
      <thead>
        <tr>
          <th>Event Name</th>
          <th>Short Description</th>
          <th>Start Date</th>
          <th>Type</th>
          <th>Active</th>
          <th></th>
        </tr>
      </thead>
      <tbody id="sortable" style="text-align:center;">
      </tbody>
    </table>
</div>

<div class="modal fade" id="eventModal" tabindex="-1" role="dialog" aria-labelledby="eventModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="closeModal">Close</button>
            <button type="button" class="btn btn-primary" id="saveEditedEventBtn">Save</button>
            <button type="button" class="btn btn-primary" id="addEventBtn">Add</button>
            <h4 class="modal-title" id="eventModalTitle" style="text-overflow: ellipsis;overflow: hidden;"></h4>
        </div>
        <div class="modal-body">
            <form id="eventForm" method="POST">
            <div class="checkbox">
                <label>
                    <input type="checkbox" id="activeCheckbox" name="active" value="1"> Active
                </label>
            </div>
            <div class="form-group">
                <label for="code">Event Name</label>
                <input type="text" class="form-control" id="eventName" name="eventName" maxlength="500">
            </div>
            <div class="form-group">
                <label for="paymentType">Type</label>
                <select class="form-control" id="type" name="type">
                    @foreach($eventTypes as $eventType)
                    <option value="{{$eventType->type}}">{{$eventType->type}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="EventTitle">Start Date</label>
                <input type="datetime-local" class="form-control" id="startDate" name="startDate"/>
            </div>
            <div class="form-group">
                <label for="EventTitle">End Date</label>
                <input type="datetime-local" class="form-control" id="endDate" name="endDate"/>
            </div>
            <div class="form-group">
                <label for="image">Image <small style="font-weight: 100;">(maximum {{ (int)($maxFileSize/1000) }}MB file size)</small></label>
                <input type="file" id="image" name="image" accept="image/jpg,image/jpeg,image/png">
                <a href="#" id="imageViewAnchor" data-lightbox="imageView">
                    <img id="imageView" class="imageView" width="auto" height="60px"/>
                </a>
            </div>
            <div class="form-group">
                <label for="eventShortDescription">Short Description</label>
                <textarea class="form-control" id="eventShortDescription" name="eventShortDescription"rows="3"></textarea>
            </div>
            <div class="form-group">
                <label for="eventLongDescription">Long Description</label>
                <textarea class="form-control" id="eventLongDescription" name="eventLongDescription"rows="5"></textarea>
            </div>
            <div class="form-group">
                <label for="eventTnc">Terms and Condition</label>
                <textarea class="form-control" id="eventTnc" name="eventTnc" rows="5"></textarea>
            </div>
            <input type="hidden" id="eventId" name="eventId">
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Delete Confirmation -->
<div id="confirmDelete" title="Delete" style="display:none;">Confirm delete this event?</div>
<!-- End Delete -->

<script type="text/javascript">
var APP_URL = {!! json_encode(url('/')) !!};
var eventTbl;
var apiKey = '<?php echo $apiKey; ?>';

var userFeatureFlag = '<?php echo json_encode($userFeatureFlag); ?>';
userFeatureFlag = JSON.parse(userFeatureFlag);

getDataTable();

$("#eventTbl").DataTable({
  "searching": true,
  "order": [],
  "data" : [],
  "columns": [
    { data: 'eventName'},
    { data: 'eventShortDescription'},
    { data: 'startDate', render: function(data, type, full){
      if(data){
        data = data.split('.');
        return data[0];
      }
      else
        return '-';
    }},
    { data: 'type'},
    { data: 'isActive', render: function(data, type, full){
        var bullet = '•';
        if(data == 1)
            return bullet.fontcolor("green").fontsize(5);
        if(data == 0)
            return bullet.fontcolor("red").fontsize(5);
    }},
    { data: 'eventId',render: function(data,type,full){
       var string = '';

        string += '<button class="btn btn-success viewEventButton" title="View" name="viewEventButton"><i class="fa fa-eye" aria-hidden="true"></i></button>';

       if(userFeatureFlag['AllowEdit'] == 1){
        string += '<button class="btn btn-warning editEventButton" title="Edit" name="editEventButton"><i class="glyphicon glyphicon-edit"></i></button>';
       }

       if(userFeatureFlag['AllowDelete'] == 1){
        string += '<button class="btn btn-danger deleteEventButton" title="Delete" data-type-code="'+data+'" name="deleteEventButton"><i class="glyphicon glyphicon-trash"></i></button>';
       }

       if(userFeatureFlag['AllowAdd'] == 1){
        string += '<button class="btn btn-primary copyEventButton" title="Copy" name="copyEventButton"><i class="fa fa-files-o" aria-hidden="true"></i></button>';
       }

      if(string == '')
        return '-';
      else
        return string;
    }}],
    "fnRowCallback" : function(nRow, aData, iDisplayIndex, iDisplayIndexFull){
      if(aData['active'] == 0){
        $('td', nRow).css('background-color', 'rgba(211, 211, 211, 0.63)');
      }
    }
});

$("#startDate").change(function(){
    var defaultDate=$('#startDate').val();
    $('#endDate').val(defaultDate);
});

$('#openAddEventBtn').click(function(event){
    //open modal
    $("#eventForm :input").prop("disabled", false);
    $("#eventForm :input").prop("readonly", false);
    $('#saveEditedEventBtn').css({'display': 'none'});
    $('#editEventBtn').css({'display': 'none'});
    $('#addEventBtn').css({'display': 'inline-block'});
    $('#eventForm')[0].reset();
    $('#imageView').attr({'src':APP_URL+'/images/noImage.png'});
    $('#imageViewAnchor').attr({'href':APP_URL+'/images/noImage.png'});
    $('#eventModalTitle').text('Add New Event');
    $('#addEventBtn').data('copy-code','');

    $('#eventModal').modal('toggle');
});

$(function(){
  $(document).on('click', 'button[name="viewEventButton"]', function(){
    $('#addEventBtn').css({'display': 'none'});
    $('#saveEditedEventBtn').css({'display': 'none'});
    var row = $(this).closest('tr');
    var data = $('#eventTbl').DataTable().row(row).data();
    fillData(data);
    // saveAudit(moduleName,data.code,'View',apiKey);
    $('#eventModalTitle').text(data.event);
    $("#eventForm :input").prop("disabled", true);
  })
});

$(function(){
  $(document).on('click', 'button[name="editEventButton"]', function(){
    $('#addEventBtn').css({'display': 'none'});
    $('#saveEditedEventBtn').css({'display': 'inline-block'});
    var row = $(this).closest('tr');
    var data = $('#eventTbl').DataTable().row(row).data();
    fillData(data);
    $('#eventModalTitle').text(data.event);
    $("#eventForm :input").prop("disabled", false);
  })
});

$(function(){
  $(document).on('click', 'button[name="deleteEventButton"]', function(){
    var id = $(this).data('type-code');
    $('#confirmDelete').data('id', id);
    $("#confirmDelete").dialog("open");
  })
});

$(function(){
  $(document).on('click', 'button[name="copyEventButton"]', function(){
    $('#addEventBtn').css({'display': 'inline-block'});
    $('#saveEditedEventBtn').css({'display': 'none'});
    var row = $(this).closest('tr');
    var data = $('#eventTbl').DataTable().row(row).data();
    fillData(data);
    $('#eventModalTitle').text('Add New Event');
    $("#eventForm :input").prop("disabled", false);
  })
});

$('#image').on("change", function(){
  if($(this)[0].files[0]){
    var reader = new FileReader();
    reader.onload = function (e) {
        $('#imageView').attr('src', e.target.result);
        $('#imageViewAnchor').attr('href', e.target.result);
    }
     reader.readAsDataURL($(this)[0].files[0]);
  }
});

function fillData(data){
    $('#eventId').val(data.eventId);
    $('#eventName').val(data.eventName);
    $('#eventShortDescription').val(data.eventShortDescription);
    $('#eventLongDescription').val(data.eventLongDescription);
    $('#eventTnc').val(data.eventTnc);
    $('#type').val(data.type);
    $('#addEventBtn').data('copy-code', data.rewardId);
    $('#createdAt').val(data.createdAt);
    $('#startDate').val(moment(data.startDate).format('YYYY-MM-DDTHH:mm:ss'));
    $('#endDate').val(moment(data.endDate).format('YYYY-MM-DDTHH:mm:ss'));

    if(data.isActive == 1)
        $("#activeCheckbox").prop('checked', true);
    else
        $("#activeCheckbox").prop('checked', false);

    if(data.imageInBase64){
        $('#imageViewAnchor').attr({'href':'data:image/'+ data.imageExtension +';base64,'+ data.imageInBase64});
        $('#imageView').attr({'src':'data:image/'+ data.imageExtension +';base64,'+ data.imageInBase64});
    }else{
        $('#imageView').attr({'src':APP_URL+'/images/noImage.png'});
        $('#imageViewAnchor').attr({'href':APP_URL+'/images/noImage.png'});
    }

    $('#eventModal').modal('toggle');
}

$('#addEventBtn').click(function(event){
    addEvent();
});

$('#saveEditedEventBtn').click(function(event){
    editEvent();
});

function addEvent(){
  //add
  var copyCode = $('#addEventBtn').data('copy-code');
  $('.overlay').css({'display':'block'});
  var formData = new FormData($('#eventForm')[0]);
  formData.append('apiKey',apiKey);
  $.ajax({
      type: "POST",
      url: APP_URL+"/api/Event/addEvent",
      data: formData,
      contentType: false,
      cache: false,
      processData:false,
      success: function(data)
      {
        if(data.error == false){
          $('#eventModal').modal('toggle');
          //save to audit
          var id = $('#code').val();
        //   saveAudit(moduleName,id,'Add',apiKey);
          alertDialog(data.message);
          getDataTable();
        }else
          alert(data.message);
        $('.overlay').css({'display':'none'});
      },error: function(){
        alert('Problem occur. Please try again');
        $('.overlay').css({'display':'none'});
      }
  });
}

function deleteEvent(id){
  $('.overlay').css({'display':'block'});
  $.ajax({
        type: "POST",
        url: APP_URL+"/api/Event/deleteEvent",
        data : { 'apiKey': apiKey ,'eventId' : id},
        success: function(data)
        {
          if(data.error == false){
            alertDialog(data.message);
            // saveAudit(moduleName,id,'Delete',apiKey);
            getDataTable();
          }else{
             alert(data.message);
          }
          $('.overlay').css({'display':'none'});
        },
        error: function(){
          alert('Error Occured');
          $('.overlay').css({'display':'none'});
        }
  });
}

function editEvent(){
  //edit
  $('.overlay').css({'display':'block'});
  var formData = new FormData($('#eventForm')[0]);
  formData.append('apiKey',apiKey);
  $.ajax({
      type: "POST",
      url: APP_URL+"/api/Event/editEvent",
      data: formData,
      contentType: false,
      cache: false,
      processData:false,
      success: function(data)
      {
        if(data.error == false){
          $('#eventModal').modal('toggle');
          //save to audit
          var id = $('#eventId').val();
        //   saveAudit(moduleName,id,'Update',apiKey);
          alertDialog(data.message);
          getDataTable();
        }else
          alert(data.message);
        $('.overlay').css({'display':'none'});
      },error: function(){
        alert('Problem occur. Please try again');
        $('.overlay').css({'display':'none'});
      }
  });
}

function getDataTable(){
  document.getElementById("loader").style.display = "block";
  document.getElementById("eventTbl").style.display = "none";

  $.ajax({
        type: "GET",
        url: APP_URL+"/api/Event/getEvents",
        data: $.param({'apiKey':apiKey}),
        success: function(data){
            document.getElementById("loader").style.display = "none";
            document.getElementById("eventTbl").style.display = "inline-table";

            var returnedData = data;
            var data = data['data'];
            if(data.length < 1){
                $('#eventTbl').dataTable().fnClearTable();
            }else{
                $('#eventTbl').dataTable().fnClearTable();
                $('#eventTbl').dataTable().fnAddData(data);
            }
        },error: function(jqXHR, exception){
          if(jqXHR.status == 500){
                document.getElementById("loader").style.display = "block";
                location.reload();
           }else{
            $('#eventTbl').dataTable().fnClearTable();
            document.getElementById("loader").style.display = "none";
            document.getElementById("eventTbl").style.display = "inline-table";
            alert('Error Occured');
           }
        }
    });
}

$("#confirmDelete").dialog({
    autoOpen: false,
    modal: true,
    buttons : {
        "Yes" : function() {
        deleteEvent($("#confirmDelete").data('id'));
        $(this).dialog("close");
        },
        "Cancel" : function() {
        $(this).dialog("close");
        }
    }
});

function checkFileSize(){
  if($("#image").val() != ''){
    var maxFileSize = parseInt('<?php echo $maxFileSize; ?>');
    var size = $('#image')[0].files[0].size;
    size = size/1000;
    if(size > maxFileSize){
      alert('File uploaded exceed maximum file size allowed')
      return 0;
    }
  }
  return 1;
}

</script>
@stop
