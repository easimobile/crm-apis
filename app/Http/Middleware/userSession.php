<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use DB;

class userSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Session::has('apiKey')){
            $apiKey = Session::get('apiKey');

            $userCode = DB::table('apiAuth')->where('apiKey',$apiKey)->value('userId');

            $check = DB::table('EGSecurityWeb')
            ->join('EGROUP','EGROUP.UGroupCode','=','EGSecurityWeb.UGroupCode')
            ->join('EPROGRAMWeb','EPROGRAMWeb.ProgramName','=','EGSecurityWeb.ProgramName')
            ->where('EGROUP.UserCode',$userCode)
            ->where('EGSecurityWeb.AllowYesNo',1)
            ->where('EPROGRAMWeb.ParentProgramName',NULL)
            ->where('EPROGRAMWeb.Active',1)
            ->orderBy('EPROGRAMWeb.MenuSequence','asc')
            ->select('EGSecurityWeb.ProgramName')
            ->first();

            if($check){

                $checkIfHasChild = DB::table('EPROGRAMWeb')
                ->join('EGSecurityWeb','EPROGRAMWeb.ProgramName','=','EGSecurityWeb.ProgramName')
                ->join('EGROUP','EGROUP.UGroupCode','=','EGSecurityWeb.UGroupCode')
                ->where('EPROGRAMWeb.Active',1)
                ->where('EGROUP.UserCode',$userCode)
                ->where('ParentProgramName',$check->ProgramName)
                ->where('AllowYesNo',1)
                ->orderBy('EPROGRAMWeb.MenuSequence','asc')
                ->select('EGSecurityWeb.ProgramName')
                ->first();

                if($checkIfHasChild){
                    $checkIfHasSecondChild = DB::table('EPROGRAMWeb')
                    ->join('EGSecurityWeb','EPROGRAMWeb.ProgramName','=','EGSecurityWeb.ProgramName')
                    ->join('EGROUP','EGROUP.UGroupCode','=','EGSecurityWeb.UGroupCode')
                    ->where('EPROGRAMWeb.Active',1)
                    ->where('EGROUP.UserCode',$userCode)
                    ->where('ParentProgramName',$checkIfHasChild->ProgramName)
                    ->where('AllowYesNo',1)
                    ->orderBy('EPROGRAMWeb.MenuSequence','asc')
                    ->select('EGSecurityWeb.ProgramName')
                    ->first();

                    if($checkIfHasSecondChild){
                        $route = $check->ProgramName.'/'.$checkIfHasChild->ProgramName.'/'.$checkIfHasSecondChild->ProgramName;
                        return redirect($route);
                    }else{
                        //$route = $checkIfHasChild->ProgramName;
                        $route = $check->ProgramName.'/'.$checkIfHasChild->ProgramName;
                        return redirect($route);
                    }
                }else{
                    $route = $check->ProgramName;
                    return redirect($route);
                }
            }else
                return redirect('contactUs');
        }
        
        return $next($request);
    }
}
