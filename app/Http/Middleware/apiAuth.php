<?php

namespace App\Http\Middleware;

use Closure;
use Response;
use Request;
use DB;

class apiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next){
        $apiKey = Request::get('apiKey');

        if(strlen($apiKey) != 128){
            return Response::json(array(
                'error' => true,
                'code' => '404',
                'message' => "Invalid key."
            ));
        }

       if(!$apiKey){
            return Response::json(array(
                'error' => true,
                'code' => '404',
                'message' => "Account not found."
            ));
        }else{
            //first check apiKey
            $check = DB::table('apiAuth')->where('apikey',$apiKey)->first(); 

            if(!$check)
            {
                return Response::json(array(
                    'error' => true,
                    'code' => '440',
                    'message' => "Account not found"
                ));
            }else{
                $userCode = $check->userId;
                $latestKey = DB::table('apiAuth')->where('userId',$userCode)->latest('createdAt')->value('apiKey');

                if($latestKey != $apiKey){
                    return Response::json(array(
                        'error' => true,
                        'code' => '440',
                        'message' => "Session Timeout"
                    ));  
                }
            }
        }
        return $next($request);
    } 
}
