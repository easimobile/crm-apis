<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\general\globalController as globalController;
use DB;
use Request, View;
use Illuminate\Support\Facades\Input;
use Response;
use Session;
use Validator;
use File;
use Log;

ignore_user_abort(1);

class ApiController extends BaseController
{
	public function __construct()
    {
        $this->employeeKeyPhrase = "3mP";
        $this->globalCtrl = new globalController();
        // $host = DB::table('sysflag')->where('flag','E22')->value('setting');

    }

    public function _getCustomerInfo($apiKey = null){
        try{
            $userCode = DB::table('CustomerApiAuth')->where('apiKey',$apiKey)->value('userId');

            $userInfo = Db::table('customer')
                        ->where('CUST_NO',$userCode)
                        ->first();

            return $userInfo;
        } catch(Exception $e) {
            return '';
        }
    }

    public function _getUserInfo($apiKey = null){

        try{
            $userCode = DB::table('apiAuth')->where('apiKey',$apiKey)->value('userId');

            $userInfo = Db::table('employee')
            ->leftJoin('OutletEmployee','OutletEmployee.EmpCode','=','employee.EMP_CD')
            ->where('employee.EMP_CD',$userCode)
            ->select('EMP_CD as userCode','EMP_NM as name','OutletEmployee.SaOut as outletCode','employee.emp_grp as userProfileCode')
            ->first();

            $userProfileGroup = DB::table('egroupCode')
            ->where('ugroupcode',$userInfo->userProfileCode)
            ->value('ugroupdesc');
            $userInfo->userProfile = $userProfileGroup;

            return $userInfo;
        } catch(Exception $e) {
            return '';
        }
    }

    public function getUserInfo(){
        $apiKey = Request::get('apiKey');

        try{
            $userCode = DB::table('apiAuth')->where('apiKey',$apiKey)->value('userId');

            $userInfo = Db::table('employee')
            ->join('OutletEmployee','OutletEmployee.EmpCode','=','employee.EMP_CD')
            ->join('SA_OUT','OutletEmployee.SaOut','=','SA_OUT.SA_OL_CD')
            ->where('employee.EMP_CD',$userCode)
            ->select('EMP_CD as userCode','EMP_NM as name','OutletEmployee.SaOut as outletCode','employee.emp_grp as userProfileCode')
            ->orderBy('SA_OUT.SA_OL_NM','asc')->first();

            $userProfileGroup = DB::table('egroupCode')
            ->where('ugroupcode',$userInfo->userProfileCode)
            ->value('ugroupdesc');
            $userInfo->userProfile = $userProfileGroup;

            return Response::json(array(
                'error' => false,
                'code' => 200,
                'message' => "User info retrieved successfully",
                'data'=>$userInfo
            ));
        } catch(Exception $e) {
            DB::rollback();

            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => "Fail to retrieved user info"
            ));
        }
    }

    public function login(){
    	$id = Input::get('id');
    	$toEncrypt = Input::get('password');
    	$keyPhrase = Input::get('keyPhrase');

		/* $checkFlag = $this->retrieveSysFlagSetting("WN0");
    	if($checkFlag == 'T'){
    		//use active diretory user
    		$adServer = DB::table('sysflag')->where('flag','WN1')->value('setting');
			$ldap = ldap_connect($adServer);
			$username = $id;
			$password = $toEncrypt;
			$ldaprdn =  $username;

			ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
			ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);

			$bind = @ldap_bind($ldap, $ldaprdn, $password);

			if(!$bind){
                return Response::json(array(
                    'error' => true,
                    'code' => 400,
                    'message' => "Invalid user id or password"
                ));
            }

			if($bind){
				$user = DB::table('employee')->where('EMP_ID',$ldaprdn)->where('Active',1)->first();

				if(!$user){
                    return Response::json(array(
                        'error' => true,
                        'code' => 400,
                        'message' => "Wrong Active Directory username"
                    ));
				}else{
				    $employeeCD = $user->EMP_CD;
				}

				$apiKey = $this->globalCtrl->apiauth();
                DB::beginTransaction();
                try{
                    DB::table('apiAuth')->insert(array('userId'=>$employeeCD,'apikey'=>$apiKey));
                    DB::commit();
                    return Response::json(array(
                        'error' => false,
                        'code' => 200,
                        'message' => "Login Successfully",
                        'apiKey' => $apiKey
                        //'data' => $userInfo
                    ));
                }catch(Exception $e) {
                    DB::rollback();
                    return Response::json(array(
                        'error' => true,
                        'code' => 400,
                        'message' => "Fail to login",
                    ));
                }
			}
		} */

		// if($checkFlag == 'F'){
			$user = DB::table('employee')->where('EMP_CD',$id)->where('Active',1)->first();

			if(!$user){
				return Response::json(array(
                    'error' => true,
                    'code' => 400,
                    'message' => "Invalid user id or password"
                ));
			}
		// }

        if($user->PWD_FIRST == 1){
            //101
            $decryptedRandomPassword = $this->decryptPassword($user->EMP_PWD);
            if($toEncrypt == $decryptedRandomPassword){
                return Response::json(array(
                    'error' => true,
                    'code' => 202,
                    'message' => "Please reset your password"
                ));
            }else{
                return Response::json(array(
                    'error' => true,
                    'code' => 400,
                    'message' => "Invalid password. Please request for first time password from admin."
                ));
            }
        }

    	//check login flag
    	$checkFlag = $this->retrieveSysFlagSetting("E04");
    	if($checkFlag == 'T'){
    		//disable encryption for login
    		$decrypted_pw = $user->EMP_PWD;
    	}else{
    		$decrypted_pw = $this->decryptPassword($user->EMP_PWD);
    	}
    	//check end

		if($decrypted_pw == $toEncrypt){
			//check password expiry date
			$passwordExpiryDate = $user->PWD_EXPIRYDATE;
			if(!(is_null($passwordExpiryDate))){
				if($passwordExpiryDate < date('Y-m-d H:i:s')){
					return Response::json(array(
						'error' => true,
						'code'  => 401,
						'message' => 'Password Expired. Please contact your admin!'
					));
				}
			}
			//check end

            //create api auth - stop here
            $apiKey = $this->globalCtrl->apiauth();
            DB::beginTransaction();
            try{
                DB::table('apiAuth')->insert(array('userId'=>$id,'apikey'=>$apiKey));

                //prompting message
                $promptFlagDays = $this->retrieveSysFlagSetting("WC1");
                $dateBeforeExpiry = strtotime($passwordExpiryDate.' -'.$promptFlagDays.' days');
                $dateBeforeExpiry = date('Y-m-d H:i:s', $dateBeforeExpiry);
                $now = date('Y-m-d H:i:s');
                DB::commit();

                if (($now > $dateBeforeExpiry) && ($now < $passwordExpiryDate)){
                    //in between
                    return Response::json(array(
                        'error' => false,
                        'code' => 302,
                        'message' => "[NOTICE] Password is near expiration date",
                        'apiKey' => $apiKey
                    ));
                }
                //end prompt

                return Response::json(array(
                    'error' => false,
                    'code' => 200,
                    'message' => "Login Successfully",
                    'apiKey' => $apiKey
                    //'data' => $userInfo
                ));
             }catch(Exception $e) {
                DB::rollback();
                return Response::json(array(
                    'error' => true,
                    'code' => 400,
                    'message' => "Fail to login",
                ));
            }
		}else{
			return Response::json(array(
				'error' => true,
				'code' => 400,
				'message' => "Invalid user name or password"
			));
		}
		//decrytion end
    }

    public function resetPassword(){
        //error msg
        $invalidMessage = "Invalid";
        $invalidPasswordMessage = "Invalid Password";
        $passwordMinimumLength = "*password length minimum";
        $passwordCharacter = " characters.";
        $passwordMinimumLengthMsg = "Kindly note that the password's length must exceed the minimum of ";
        $passwordDoesntMatchMsg = "Confirm password does not match with password.";
        $passwordMinimumStrengthMsg = "Minimum password strength level is medium.";
        $passwordSameNameMsg = "Password cannot be the same as your code / name or contain your code / name.";
        //error msg end

        //systemFlag
        $validationPasswordCharacter = $this->retrieveSysFlagSetting('S79');
        $passUserName = $this->retrieveSysFlagSetting('H02');
        $passStrength = $this->retrieveSysFlagSetting('H03');
        //systemFlag end

        //user info
        $userId = Input::get('userId');
        $userName = DB::table('employee')->where('EMP_CD',$userId)->first();
        //user info add end

        $rules = array(
            'newPw'              => 'required|max:40|confirmed',
            'newPw_confirmation' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);
        $validateErrors = array();

        if($validator->passes()){

            //change password
            $newPw = Input::get('newPw');

            if(is_Numeric($validationPasswordCharacter)){
                //validation of password's length
                if((int)$validationPasswordCharacter > strlen($newPw)){
                    array_push($validateErrors,$passwordMinimumLengthMsg.$validationPasswordCharacter.$passwordCharacter);
                }
            }

            if($passUserName == 'T'){
                //password cannot be the same as your name or code
                if(strtoupper($newPw) == strtoupper($userId) || strtoupper($newPw) == strtoupper($userName->EMP_NM)){
                    array_push($validateErrors, $passwordSameNameMsg);
                }
            }

            if(is_Numeric($passStrength)){
                //minimum password strenth level
                //1 - very weak : plain numbers or characters
                //2 - weak : combine numbers and characters
                //3 - normal
                //4 - strong : 1 caps, 1 lower
                //5 - very strong : contains symbol
                if($passStrength >= 2 && $passStrength <= 5){
                    if(1 !== preg_match('~[0-9]~', $newPw) || !(preg_match("/[a-z]/i", $newPw))){
                        array_push($validateErrors, "Password must be combination of numbers and characters");
                    }
                }
                if($passStrength >= 4){
                    if(!(preg_match('/[A-Z]/', $newPw)) || !(preg_match("/[a-z]/", $newPw))){
                        array_push($validateErrors, "Password must include at least 1 uppercase and 1 lowercase character");
                    }

                }
                if($passStrength >= 5){
                    if(!(preg_match('/[\'^`£!$%&*()}{@#~?><>.,|=_+¬-]/',$newPw))){
                        array_push($validateErrors, "Password must include at least 1 symbol");
                    }
                }
            }

            if(count($validateErrors) > 0){
                return Response::json(array(
                    'error' => true,
                    'code' => 400,
                    'message' => $validateErrors
                ));
            }

            //encrytion
            $newEncrypted = $this->encryptPassword($newPw);

            $defaultPasswordExpiredExtensionDays = $this->retrieveSysFlagSetting('H05');
            $newExpiredDate = new DateTime("+".$defaultPasswordExpiredExtensionDays." days");
            $storeNewPw = DB::table('employee')->where('EMP_CD', $userId)->update(['EMP_PWD' => $newEncrypted,'PWD_EXPIRYDATE'=>$newExpiredDate,'PWD_FIRST'=>0]);

            if($storeNewPw){
                return Response::json(array(
                    'error' => false,
                    'code' => 200,
                    'message' => "Password has been change successfully."
                ));
            }
        }else{
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validator
            ));
        }
    }

    public function changePw(){
        $changePasswordNonApi = Request::get('changePasswordNonApi');

    	//error msg
   		$invalidMessage = "Invalid";
    	$invalidPasswordMessage = "Invalid Password";
    	$passwordMinimumLength = "*password length minimum";
    	$passwordCharacter = " characters.";
    	$passwordMinimumLengthMsg = "Kindly note that the password's length must exceed the minimum of ";
    	$passwordDoesntMatchMsg = "Confirm password does not match with password.";
    	$passwordMinimumStrengthMsg = "Minimum password strength level is medium.";
    	$passwordSameNameMsg = "Password cannot be the same as your code / name or contain your code / name.";
    	//error msg end

    	//systemFlag
    	$validationPasswordCharacter = $this->retrieveSysFlagSetting('S79');
    	$passUserName = $this->retrieveSysFlagSetting('H02');
    	$passStrength = $this->retrieveSysFlagSetting('H03');
    	//systemFlag end

    	//user info
        $apiKey = Request::get('apiKey');

        if($apiKey){
            $userInfo = $this->_getUserInfo($apiKey);
            $userId = $userInfo->userCode;
        }else{
            $userId = Request::get('userId');
        }
        $userName = DB::table('employee')->where('EMP_CD',$userId)->first();
    	//user info add end

		$rules = array(
        	'currentPw'          => 'required',
        	'newPw'              => 'required|max:40|confirmed',
        	'newPw_confirmation' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);
        $validateErrors = array();

        if($validator->passes()){
    		//change password
    		$currentPw = Input::get('currentPw');
    		$newPw = Input::get('newPw');

        	//decrypt check
        	$currentStorePassword = DB::table('employee')->where('EMP_CD', $userId)->value('EMP_PWD');
        	$decryptCurrentStorePassword = $this->decryptPassword($currentStorePassword);

        	if((string)$decryptCurrentStorePassword != $currentPw){
                array_push($validateErrors,$invalidPasswordMessage);
                if($changePasswordNonApi == 1){
                    return Redirect::back()->withErrors($validateErrors)->withInput(Input::except('newPw', 'newPw_confirmation'));
                }else{
                    return Response::json(array(
                        'error' => true,
                        'code' => '400',
                        'pw' => $currentPw,
                        'message' => $validateErrors
                    ));
                }
        	}
        	//decrypt end

        	if(is_Numeric($validationPasswordCharacter)){
        		//validation of password's length
        		if((int)$validationPasswordCharacter > strlen($newPw)){
        			array_push($validateErrors,$passwordMinimumLengthMsg.$validationPasswordCharacter.$passwordCharacter);
        		}

        	}

    		if($passUserName == 'T'){
    			//password cannot be the same as your name or code
    			if(strtoupper($newPw) == strtoupper($userId) || strtoupper($newPw) == strtoupper($userName->EMP_NM)){
    				array_push($validateErrors, $passwordSameNameMsg);
    			}
    		}
    		if(is_Numeric($passStrength)){
    			//minimum password strenth level
                //1 - very weak : plain numbers or characters
                //2 - weak : combine numbers and characters
                //3 - normal
                //4 - strong : 1 caps, 1 lower
                //5 - very strong : contains symbol
                if($passStrength >= 2 && $passStrength <= 5){
                    if(1 !== preg_match('~[0-9]~', $newPw) || !(preg_match("/[a-z]/i", $newPw))){
                        array_push($validateErrors, "Password must be combination of numbers and characters");
                    }
                }
                if($passStrength >= 4){
                    if(!(preg_match('/[A-Z]/', $newPw)) || !(preg_match("/[a-z]/", $newPw))){
                        array_push($validateErrors, "Password must include at least 1 uppercase and 1 lowercase character");
                    }

                }
                if($passStrength >= 5){
                    if(!(preg_match('/[\'^`£!$%&*()}{@#~?><>.,|=_+¬-]/',$newPw))){
                        array_push($validateErrors, "Password must include at least 1 symbol");
                    }
                }
    		}

    		if(count($validateErrors) > 0){
                if($changePasswordNonApi == 1){
                    return Redirect::back()->withErrors($validateErrors)->withInput(Input::except('newPw', 'newPw_confirmation'));
                }else{
    				return Response::json(array(
        				'error' => true,
        				'code' => 400,
        				'message' => $validateErrors
        			));
                }
    		}

			//encrytion
			$newEncrypted = $this->encryptPassword($newPw);
            $defaultPasswordExpiredExtensionDays = $this->retrieveSysFlagSetting('H05');
            $newExpiredDate = new DateTime("+".$defaultPasswordExpiredExtensionDays." days");
    		$storeNewPw = DB::table('employee')->where('EMP_CD', $userId)->update(['EMP_PWD' => $newEncrypted,'PWD_EXPIRYDATE'=>$newExpiredDate,'PWD_FIRST'=>0]);

    		if($storeNewPw){
                $message = "Password has been change successfully.";
                if($changePasswordNonApi == 1){
                    return Redirect::back()->with('success',$message);
                }else{
                    return Response::json(array(
                        'error' => false,
                        'code' => 200,
                        'message' => $message
                    ));
                }
    		}
        }else{
            if($changePasswordNonApi == 1){
                return Redirect::back()->withErrors($validator)->withInput(Input::except('newPw', 'newPw_confirmation'));
            }else{
                return Response::json(array(
                    'error' => true,
                    'code' => 400,
                    'message' => $validator
                ));
            }
        }
    }

    public function apiauth(){
        $bytes = openssl_random_pseudo_bytes(64, $cstrong);
        $apikey = bin2hex($bytes);

        if(!$cstrong){
            return 0;
        } else {
            return $apikey;
        }
    }

	public function md5hash($keyPhrase){
		$key = mb_convert_encoding($keyPhrase, "ASCII");
		$key = md5($key,true);
    	$key .= substr($key, 0, 8);
    	return $key;
	}

	public function retrieveSysFlagSetting($flag){
        //for internal api use
		$setting = DB::table('sysflag')->where('flag',$flag)->value('setting');
		return $setting;
	}

	public function encryptPassword($toEncrypt = null){

        if(Request::get('toEncrypt'))
            $toEncrypt = Request::get('toEncrypt');

        $key = $this->md5hash($this->employeeKeyPhrase);
        $iv = "";
        $newEncrypted = openssl_encrypt($toEncrypt, 'des-ede3', $key, 0, $iv);

        return $newEncrypted;
	}
	 public function decryptPassword($encrypted_pw = null){

        if(Request::get('encrypted_pw'))
            $encrypted_pw = Request::get('encrypted_pw');

        $key = $this->md5hash($this->employeeKeyPhrase);
        $iv = '';

        $decrypted_pw = openssl_decrypt($encrypted_pw, 'des-ede3', $key, 0, $iv);

        return $decrypted_pw;
	}

    public function getSysflagValue(){
        $sysflag = Request::get('sysflag');
        $data = DB::table('sysflag')->where('flag',$sysflag)->select('setting')->first();

        if($data){
            return Response::json(array(
                'error' => false,
                'code' => 200,
                'message' => "Sysflag retrieve successfully",
                'data' => $data
            ));
        }else{
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => "Sysflag not found"
            ));
        }
    }


    public function _insertEventLogger($eventLoggerType, $eventLoggerFunction, $eventLoggerLog,$eventLoggerDestination){
        try{
            $eventLogArray = array('EventLoggerType'=>$eventLoggerType,'EventLoggerFunction'=>$eventLoggerFunction,'EventLoggerLog'=>$eventLoggerLog,'EventLoggerTime'=>date('Y-m-d H:i:s'),'EventLoggerDestination'=>$eventLoggerDestination);

            DB::table('eventLogger')->insert($eventLogArray);

        }catch(Exception $e){
             Log::error($e->getMessage());
        }
    }

    public function generate_referralCode(){
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $res = "";
        for ($i = 0; $i < 8; $i++) {
            $res .= $chars[mt_rand(0, strlen($chars)-1)];
        }
        return $res;
    }

    public function generate_code(){
        $chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $res = "";
        for ($i = 0; $i < 11; $i++) {
            $res .= $chars[mt_rand(0, strlen($chars)-1)];
        }
        return $res;
    }
}
