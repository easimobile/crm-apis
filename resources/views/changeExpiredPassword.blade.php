<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>{{ $tabTitle }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <!-- Bootstrap 3.3.2 -->
    <link href="{{ asset('AdminLTE/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    
    <!-- Font Awesome Icons -->
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    
    <!-- Ionicons -->
    <link href="{{ asset('css/ionicons.min.css') }}" rel="stylesheet" type="text/css"/>

    <link href="{{ asset('AdminLTE/dist/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('AdminLTE/dist/css/skins/skin-blue.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('AdminLTE/dist/css/skins/skin-red.min.css') }}" rel="stylesheet" type="text/css" />
    
    <!--js-->
    <script src="{{ asset('js/jquery/1.11.0/jquery.min.js') }}"></script>

    <!-- jquery ui -->
    <link rel="stylesheet" href="{{ asset('css/1.12.1/jquery-ui.css') }}"/>
    <script src="{{ asset('js/jquery/1.12.1/jquery-ui.js') }}"></script>

    <!-- jquery validate plugin -->
    <script src="{{ asset('js/jquery.validate.js') }}"></script>
    <script src="{{ asset('js/additional-methods.js') }}"></script>

    <!-- css -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    {{ HTML::script( asset('AdminLTE/bootstrap/js/bootstrap.min.js')) }} 
    {{ HTML::script( asset('AdminLTE/dist/js/app.min.js')) }}
  </head>
  <body>
    <div>
      <div>
        <section class="content">
            <div class="login-box" style="width: 510px">
			  <div class="login-logo">
				<b> </b>Change Expired Password
			  </div>
			  <div class="login-box-body">
				<div>
					@if ($errors->has())
						<div class="alert alert-danger">
							@foreach ($errors->all() as $error)
								{{ $error }}<br>        
							@endforeach
						</div>
					@elseif(Session::has('success'))
						<div class="alert alert-success" id="success-alert">
							<a href="#" class="close" data-dismiss="alert">&times;</a>
							{{ Session::get('success') }}
						</div>
					@elseif(Session::has('fail'))
						<div class="alert alert-danger">
							<a href="#" class="close" data-dismiss="alert">&times;</a>
							{{ Session::get('fail') }}
						</div>
					@elseif (Session::has('message'))
						<div class='bg-danger alert'>{{ Session::get('message') }}</div>
					@endif            
				</div><!-- /.col -->

				 <form class="form-horizontal" method="post" id="changeExpiredPasswordForm" action="{{ asset('changeExpiredPassword') }}">
          <div class="box-body">
          <div class="form-group">
            <label for="userCode" class="col-sm-2 control-label">User ID</label>
            <div class="col-sm-10">
              <?php if(old('userCode'))
                      $userCode = old('userCode');
                    else{
                      $userCode = Session::get('userCode');
              } ?>
              <input type="text" class="form-control" id="userCode" name="userCode" value="{{ $userCode }}" readonly>
            </div>
          </div>
          <div class="form-group">
            <label for="currentPassword" class="col-sm-2 control-label">Current Password</label>
            <div class="col-sm-10">
              <input type="password" class="form-control" id="currentPassword" name="currentPassword">
            </div>
          </div>
          <div class="form-group">
            <label for="newPassword" class="col-sm-2 control-label">New Password</label>
            <div class="col-sm-10">
              <input type="password" class="form-control" id="newPassword" name="newPassword">
            </div>
          </div>
          <div class="form-group">
            <label for="newPassword_confirmation" class="col-sm-2 control-label">Confirm Password</label>
              <div class="col-sm-10">
                <input type="password" class="form-control" id="newPassword_confirmation" name="newPassword_confirmation">
              </div>
          </div>
				  <div class="row">
					<div class="col-xs-4 pull-right text-right" style="width: 100%;text-align: center;">
					  <button type="submit" class="btn btn-danger">Change Password</button>
					</div><!-- /.col -->
				  </div>
        </div>
				</form>
			  </div>
			</div>
        </section>
      </div>
    </div>
  </body>

<script>
var APP_URL = {!! json_encode(url('/')) !!};
var session_userCode = "<?php echo Session::get('userCode'); ?>";
var old_userCode = "<?php echo old('userCode'); ?>";

if(!session_userCode){
  if(!old_userCode){
    alert('Session Expired. Please login again');
    window.location.href = APP_URL+"/login";
  }
}

$("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
    $("#success-alert").slideUp(500);
});
</script>
</html>