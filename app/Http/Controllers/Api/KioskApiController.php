<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController as ApiController;
use App\Http\Controllers\Api\MobileApiController as MobileApiController;
use App\Http\Controllers\Api\UserApiController as UserApiController;
use view;
use Input;
use Request;
use Validator;
use Response;
use Log;
use DB;
use Route;
use File;

class KioskApiController{
	public function __construct(){
        $this->apiCtrl = new ApiController();
        $this->mobileApiCtrl = new MobileApiController();
        $this->userApiCtrl = new UserApiController;
        $this->searchUser_url = "https://api.luxand.cloud/photo/search";
        $this->imageDefaultHost = 'http://118.189.155.210:8088/EASI_CRM/public/';
    }

    public function multiplier($uid){
        $data = DB::table('Customer')->select('tier', 'gainedPoints')->where('CUST_NO',$uid)->first();
        $silver = DB::table('tier')->where('name', 'Silver')->first();
        $gold = DB::table('tier')->where('name', 'Gold')->first();
        $platinum = DB::table('tier')->where('name', 'Platinum')->first();

        switch($point = $data->gainedPoints){
            case ($point >= 0 && $point <= $silver->threshold):
                if(strtolower($data->tier) == strtolower($silver->name)){
                    return (double)$silver->pointMultiplier;
                }else{
                    $data = DB::table('Customer')->where('CUST_NO',$uid)->update(array('tier'=> $silver->name));
                    return (double)$silver->pointMultiplier;
                }
            break;
            case ($point > $silver->threshold && $point <= $gold->threshold):
                if(strtolower($data->tier) == strtolower($gold->name)){
                    return (double)$gold->pointMultiplier;
                }else{
                    $data = DB::table('Customer')->where('CUST_NO',$uid)->update(array('tier'=> $gold->name));
                    return (double)$gold->pointMultiplier;
                }
            break;
            case ($point >= $platinum->threshold):
                if(strtolower($data->tier) == strtolower($platinum->name)){
                    return (double)$platinum->pointMultiplier;
                }else{
                    $data = DB::table('Customer')->where('CUST_NO',$uid)->update(array('tier'=> $platinum->name));
                    return (double)$platinum->pointMultiplier;
                }
            break;
        }
    }

    public function Transactions(){
        $destination = asset('/').Route::getCurrentRoute()->getPath();
        $requestName = "TransactionsRequest";
        $responseName = "TransactionsResponse";

        DB::beginTransaction();
        $this->apiCtrl->_insertEventLogger('kiosk', $requestName, json_encode(Request::all()),$destination);
        DB::commit();

        $rules = array(
            'uid'           => 'required',
            'outletCode'    => 'required',
            'outletName'    => 'required',
            'posNo'         => 'required',
            'receiptNo'     => 'required',
            'totalAmount'   => 'required'
            // 'status'        => 'required'
        );
        $validator = Validator::make(Request::all(), $rules);
        if($validator->passes()){
            $uid = Request::get('uid');
            $outletCode = Request::get('outletCode');
            $outletName = Request::get('outletName');
            $posNo = Request::get('posNo');
            $receiptNo = Request::get('receiptNo');
            $totalAmount = Request::get('totalAmount');
            $businessDate = Request::get('businessDate');
            $transactionDate = Request::get('transactionDate');
            $status = "PURCHASED";
            $now = date("Y-m-d H:i:s");

            try{
                DB::beginTransaction();
                $exist = DB::table('Transactions')->where('uid',$uid)->where('receiptNo',$receiptNo)->first();
                if(!$exist){
                    //Insert Transaction
                    $insertTransaction = array( 'uid'               => $uid,
                                                'outletCode'        => $outletCode,
                                                'outletName'        => $outletName,
                                                'posNo'             => $posNo,
                                                'receiptNo'         => $receiptNo,
                                                'totalAmount'       => $totalAmount,
                                                'paymentStatus'     => $status,
                                                'businessDate'      => $businessDate,
                                                'transactionDate'   => $transactionDate
                    );
                    DB::table('Transactions')->insert($insertTransaction);

                    //Insert Point history
                    $amt = (int)$totalAmount;
                    $pointMultiplier = $this->multiplier($uid);
                    $point = $amt * $pointMultiplier;

                    $transactionId = DB::table('Transactions')->where('receiptNo',$receiptNo)->where('uid',$uid)->value('transactionId');
                    $insertPoint = array('uid'              => $uid,
                                        'insertDate'        => $now,
                                        'transactionDate'   => $transactionDate,
                                        'transactionId'     => $transactionId,
                                        'outletCode'        => $outletCode,
                                        'outletName'        => $outletName,
                                        'point'             => $point,
                                        'type'              => 'POINT_ISSUANCE',
                                        'isExpired'         => 0
                    );
                    DB::table('Point')->insert($insertPoint);

                    //Update customer point
                    $data = DB::table('Customer')->where('CUST_NO',$uid)->select('usablePoints','availablePoints','gainedPoints')->first();
                    $usablePoints = $data->usablePoints;
                    $availablePoints = $data->availablePoints;
                    $gainedPoints = $data->gainedPoints;
                    $usablePoints += $point;
                    $availablePoints += $point;
                    $gainedPoints += $point;
                    DB::table('Customer')->where('CUST_NO',$uid)->update(array('usablePoints'=>$usablePoints,'availablePoints'=>$availablePoints,'gainedPoints'=>$gainedPoints));
                    $this->userApiCtrl->checkTier($uid);

                    //Send Notification
                    $messageContent = DB::table('notificationMessage')->where('category','PURCHASE')->first();
                    $title = $messageContent->title;
                    $body = $messageContent->body;
                    if(strpos($body, '[AMOUNT]') !== false) {
                        $body = str_replace("[AMOUNT]",$totalAmount,$body);
                    }
                    if(strpos($body, '[POINT]') !== false) {
                        $body = str_replace("[POINT]",$point,$body);
                    }
                    $this->mobileApiCtrl->_sendNotification($title,$body,'PURCHASE',$uid);
                }else{
                    return Response::json(array(
                        'error'     => true,
                        'code'      => 200,
                        'message'   => 'This transaction already save.'
                    ));
                }

                $this->apiCtrl->_insertEventLogger('kiosk', $responseName,'Transaction added successfully',$destination);
                DB::commit();
                return Response::json(array(
                    'error'     => false,
                    'code'      => 200,
                    'message'   => 'OK'
                ));
            }catch(Exeption $e){
                DB::rollBack();
                Log::info($e);
                $this->apiCtrl->_insertEventLogger('kiosk', $responseName, $e->getMessage(), $destination);
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => $e->getMessage()
                ));
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            Log::info($validationErrorString);
            $this->apiCtrl->_insertEventLogger('kiosk', $responseName, $validationErrorString, $destination);
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }

    public function TransactionsSyncByFace(){
        $destination = asset('/').Route::getCurrentRoute()->getPath();
        $requestName = "TransactionsSyncByFaceRequest";
        $responseName = "TransactionsSyncByFaceResponse";

        DB::beginTransaction();
        $this->apiCtrl->_insertEventLogger('kiosk', $requestName, json_encode(Request::all()),$destination);
        DB::commit();

        $rules = array(
            'face'          => 'required',
            'outletCode'    => 'required',
            'outletName'    => 'required',
            'posNo'         => 'required',
            'receiptNo'     => 'required',
            'totalAmount'   => 'required'
        );
        $validator = Validator::make(Request::all(), $rules);
        if($validator->passes()){
            $face = Request::get('face');
            $outletCode = Request::get('outletCode');
            $outletName = Request::get('outletName');
            $posNo = Request::get('posNo');
            $receiptNo = Request::get('receiptNo');
            $totalAmount = Request::get('totalAmount');
            $businessDate = Request::get('businessDate');
            $transactionDate = Request::get('transactionDate');
            $status = "PURCHASED";
            $now = date("Y-m-d H:i:s");

            try{
                DB::beginTransaction();
                $temp_id = $this->apiCtrl->generate_code();
                if(!file_exists('images/customer_face_temp/')) {
                    mkdir('images/customer_face_temp/', 0777, true);
                }
                $decode = base64_decode($face);
                file_put_contents('images/customer_face_temp/'.$temp_id.'.jpg',$decode);
                $face_url = $this->imageDefaultHost.'images/customer_face_temp/'.$temp_id.'.jpg';
                $userFace = array("photo" => $face_url);

                $response = $this->userApiCtrl->service($this->searchUser_url, 'POST', $userFace, $requestName.'(TransactionsSyncByFace)', $responseName.'(TransactionsSyncByFace)');
                $response = json_decode($response);
                if(!empty($response)){
                    if(isset($response[0]->id)){
                        if(isset($response[0]->name)){
                            $uid = $response[0]->name;
                            $personId = $response[0]->id;
                            $data = DB::table('customer')->where('CUST_NO', $uid)->where('personId', $personId)->first();

                            File::delete('images/customer_face_temp/'.$temp_id.'.jpg');
                            if($data){
                                $uid = $data->CUST_NO;
                            }else{
                                DB::commit();
                                return Response::json(array(
                                    'error'     => true,
                                    'code'      => 500,
                                    'message'   => 'User not found.'
                                ));
                            }
                        }
                    }
                }else{
                    File::delete('images/customer_face/'.$temp_id.'.jpg');
                    DB::commit();
                    return Response::json(array(
                        'error'     => true,
                        'code'      => 500,
                        'message'   => 'User not found.'
                    ));
                }

                $exist = DB::table('Transactions')->where('uid',$uid)->where('receiptNo',$receiptNo)->first();
                if(!$exist){
                    //Insert Transaction
                    $insertTransaction = array( 'uid'               => $uid,
                                                'outletCode'        => $outletCode,
                                                'outletName'        => $outletName,
                                                'posNo'             => $posNo,
                                                'receiptNo'         => $receiptNo,
                                                'totalAmount'       => $totalAmount,
                                                'paymentStatus'     => $status,
                                                'businessDate'      => $businessDate,
                                                'transactionDate'   => $transactionDate
                    );
                    DB::table('Transactions')->insert($insertTransaction);

                    //Insert Point history
                    $amt = (int)$totalAmount;
                    $pointMultiplier = $this->multiplier($uid);
                    $point = $amt * $pointMultiplier;

                    $transactionId = DB::table('Transactions')->where('receiptNo',$receiptNo)->where('uid',$uid)->value('transactionId');
                    $insertPoint = array('uid'              => $uid,
                                        'insertDate'        => $now,
                                        'transactionDate'   => $transactionDate,
                                        'transactionId'     => $transactionId,
                                        'outletCode'        => $outletCode,
                                        'outletName'        => $outletName,
                                        'point'             => $point,
                                        'type'              => 'POINT_ISSUANCE',
                                        'isExpired'         => 0
                    );
                    DB::table('Point')->insert($insertPoint);

                    //Update customer point
                    $data = DB::table('Customer')->where('CUST_NO',$uid)->select('usablePoints','availablePoints','gainedPoints')->first();
                    $usablePoints = $data->usablePoints;
                    $availablePoints = $data->availablePoints;
                    $gainedPoints = $data->gainedPoints;
                    $usablePoints += $point;
                    $availablePoints += $point;
                    $gainedPoints += $point;
                    DB::table('Customer')->where('CUST_NO',$uid)->update(array('usablePoints'=>$usablePoints,'availablePoints'=>$availablePoints,'gainedPoints'=>$gainedPoints));
                    $this->userApiCtrl->checkTier($uid);

                    //Send Notification
                    $messageContent = DB::table('notificationMessage')->where('category','PURCHASE')->first();
                    $title = $messageContent->title;
                    $body = $messageContent->body;
                    if(strpos($body, '[AMOUNT]') !== false) {
                        $body = str_replace("[AMOUNT]",$totalAmount,$body);
                    }
                    if(strpos($body, '[POINT]') !== false) {
                        $body = str_replace("[POINT]",$point,$body);
                    }
                    $this->mobileApiCtrl->_sendNotification($title,$body,'PURCHASE',$uid);
                }else{
                    return Response::json(array(
                        'error'     => true,
                        'code'      => 200,
                        'message'   => 'This transaction already save.'
                    ));
                }

                $this->apiCtrl->_insertEventLogger('kiosk', $responseName,'Transaction added successfully',$destination);
                DB::commit();
                return Response::json(array(
                    'error'     => false,
                    'code'      => 200,
                    'message'   => 'OK'
                ));
            }catch(Exeption $e){
                DB::rollBack();
                Log::info($e);
                $this->apiCtrl->_insertEventLogger('kiosk', $responseName, $e->getMessage(), $destination);
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => $e->getMessage()
                ));
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            Log::info($validationErrorString);
            $this->apiCtrl->_insertEventLogger('kiosk', $responseName, $validationErrorString, $destination);
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }

    public function claimReward(){
        $destination = asset('/').Route::getCurrentRoute()->getPath();
        $requestName = "ClaimRewardRequest";
        $responseName = "ClaimRewardResponse";

        DB::beginTransaction();
        $this->apiCtrl->_insertEventLogger('kiosk', $requestName, json_encode(Request::all()),$destination);
        DB::commit();

        $rules = array(
            'apiKey'        => 'required',
            'uid'           => 'required|exists:customer,cust_no',
            'outletCode'    => 'required',
            'outletName'    => 'required',
            'posNo'         => 'required',
            'userRewardId'  => 'required|exists:UserReward,id'
        );
        $validator = Validator::make(Request::all(), $rules);
        if($validator->passes()){
            $userRewardId = Request::get('userRewardId');
            $uid = Request::get('uid');
            $outletCode = Request::get('outletCode');
            $outletName = Request::get('outletName');
            $posNo = Request::get('posNo');

            try{
                $reward = DB::table('UserReward')
                        ->where('uid', $uid)
                        ->where('id', $userRewardId)
                        ->get();
                if($reward){
                    if($reward->isClaimed == false){
                        $updateArray = array('isClaimed'=> true,
                                            'outletCode'=> $outletCode,
                                            'outletName'=> $outletName,
                                            'posNo'     => $posNo,
                                            'claimDate' => date('Y-m-d H:i:s'));

                        DB::table('UserReward')
                        ->where('uid', $uid)
                        ->where('id', $userRewardId)
                        ->update($updateArray);

                        //Send Notification
                        $rewardName = DB::table('Reward')->where('rewardId',$reward->isClaimed)->value('rewardName');
                        $messageContent = DB::table('notificationMessage')->where('category','REWARD_CLAIM')->first();
                        $title = $messageContent->title;
                        $body = $messageContent->body;
                        if(strpos($body, '[REWARD_NAME]') !== false) {
                            $body = str_replace("[REWARD_NAME]",$rewardName,$body);
                        }
                        $this->mobileApiCtrl->_sendNotification($title,$body,'REWARD_CLAIM',$uid);
                    }else{
                        return Response::json(array(
                            'error'     => false,
                            'code'      => 200,
                            'message'   => 'MAX_CLAIMED_REWARD'
                        ));
                    }
                }else{
                    return Response::json(array(
                        'error'     => false,
                        'code'      => 200,
                        'message'   => 'Reward does not exist'
                    ));
                }
                DB::commit();
                return Response::json(array(
                    'error'     => false,
                    'code'      => 200,
                    'message'   => 'OK'
                ));
            }catch(Exeption $e){
                DB::rollback();
                Log::info($e);
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => $e->getMessage()
                ));
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            Log::info($validationErrorString);
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }

}
