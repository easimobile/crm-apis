<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController as ApiController;
use Firebase\Auth\Token\Exception\InvalidToken;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Factory;
use view;
use Input;
use Request;
use Validator;
use Response;
use DB;
use Route;
use Log;
use File;

class UserApiController{
	public function __construct(){
        $this->apiCtrl = new ApiController();
        $this->eventloggerType = 'API';
        $this->faceProbability = 0.9;
        $this->imageDefaultHost = 'http://118.189.155.210:8088/EASI_CRM/public/';
        $this->createPerson_url = "https://api.luxand.cloud/subject";
        $this->addFace_url = "https://api.luxand.cloud/subject/";
        $this->searchUser_url = "https://api.luxand.cloud/photo/search";
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/serviceAccountKey.json');
        $this->firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->create();
        $this->now = date("Y-m-d");
        $this->nowDT = date("Y-m-d H:i:s");
    }

    public function service($url,$customeRequest ,$arrayData,$requestName,$responseName){
        DB::beginTransaction();
        $this->apiCtrl->_insertEventLogger($this->eventloggerType, '_'.$requestName, json_encode($arrayData),$url);
        DB::commit();

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $customeRequest,
            CURLOPT_POSTFIELDS => $arrayData,
            CURLOPT_HTTPHEADER => array(
                "token: 6c3bb31bb2924663975109643b2801ac"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if($err){
            DB::beginTransaction();
            $this->apiCtrl->_insertEventLogger($this->eventloggerType, '_'.$responseName, $err, $url);
            DB::commit();
        }else{
            DB::beginTransaction();
            $this->apiCtrl->_insertEventLogger($this->eventloggerType, '_'.$responseName, $response,$url);
            DB::commit();
        }

        return $response;
    }

    public function checkTier($uid){
        $data = DB::table('Customer')->select('tier', 'gainedPoints')->where('CUST_NO',$uid)->first();
        $silver = DB::table('tier')->where('name', 'Silver')->first();
        $gold = DB::table('tier')->where('name', 'Gold')->first();
        $platinum = DB::table('tier')->where('name', 'Platinum')->first();

        switch($point = $data->gainedPoints){
            case ($point < $gold->threshold):
                if(strtolower($data->tier) != strtolower($silver->name)){
                    $data = DB::table('Customer')->where('CUST_NO',$uid)->update(array('tier'=> $silver->name));
                }
            break;
            case ($point >= $gold->threshold && $point <= $platinum->threshold):
                if(strtolower($data->tier) != strtolower($gold->name)){
                    $data = DB::table('Customer')->where('CUST_NO',$uid)->update(array('tier'=> $gold->name));
                }
            break;
            case ($point >= $platinum->threshold):
                if(strtolower($data->tier) != strtolower($platinum->name)){
                    $data = DB::table('Customer')->where('CUST_NO',$uid)->update(array('tier'=> $platinum->name));
                }
            break;
        }
    }

    public function verifyUser(){
        $rules = array(
            'uid'           => 'required'
        );
        $validator = Validator::make(Request::all(), $rules);

        if($validator->passes()){
            $uid = Request::get('uid');
            try {
                DB::beginTransaction();
                $data = DB::table('customer')->where('CUST_NO', $uid)->first();
                DB::commit();

                if($data){
                    return Response::json(array(
                        'error'     => false,
                        'code'      => 200,
                        'message'   => 'OK',
                        'data'      => 'true'
                    ));
                }else{
                    return Response::json(array(
                        'error'     => false,
                        'code'      => 200,
                        'message'   => 'OK',
                        'data'      => 'false'
                    ));
                }
            }catch(Exeption $e){
                Log::info($e->getMessage());
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => $e->getMessage()
                ));
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            Log::info($validationErrorString);
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }

    public function register(){
        $destination = asset('/').Route::getCurrentRoute()->getPath();
        $requestName = "LoginRequest";
        $responseName = "LoginResponse";

        DB::beginTransaction();
        $this->apiCtrl->_insertEventLogger($this->eventloggerType, $requestName, json_encode(Request::all()),$destination);
        DB::commit();

        $rules = array(
            'idToken'       => 'required',
            'uid'           => 'required',
            'name'          => 'required',
            'email'         => 'required',
            'country'       => 'required'
        );

        $validator = Validator::make(Request::all(), $rules);
        if($validator->passes()){
            $idToken = Request::get('idToken');
            $uid = Request::get('uid');

            try{
                $verifiedIdToken = $this->firebase->getAuth()->verifyIdToken($idToken);
                $firebase_uid = $verifiedIdToken->getClaim('sub');

                if($firebase_uid == $uid){
                    $name = Request::get('name');
                    $email = Request::get('email');
                    $country = Request::get('country');
                    $otherRC = Request::get('referralCode');
                    $profilePictureInBase64 = Request::get('profilePictureInBase64');
                    $point = 0;

                    DB::beginTransaction();
                    $check = DB::table('customer')->where('cust_no', $uid)->first();

                    if(!$check){
                        if(!empty($otherRC)){
                            $data = DB::table('customer')->select('CUST_NO as uid', 'usablePoints', 'availablePoints','gainedPoints')->where('referralCode', $otherRC)->first();
                            if($data){
                                $usablePoints = $data->usablePoints;
                                $availablePoints = $data->availablePoints;
                                $gainedPoints = $data->gainedPoints;
                                $usablePoints += 100;
                                $availablePoints += 100;
                                $gainedPoints += 100;
                                $insertPoint = array('uid'          => $data->uid,
                                                    'insertDate'    => $this->nowDT,
                                                    'point'         => 100,
                                                    'type'          => 'POINT_ISSUANCE',
                                                    'remarks'       => 'Referral',
                                                    'isExpired'     => 0
                                );
                                DB::table('point')->insert($insertPoint);
                                DB::table('customer')->where('cust_no', $data->uid)->update(array('usablePoints'=>$usablePoints,'availablePoints'=>$availablePoints,'gainedPoints'=>$gainedPoints));
                                $this->checkTier($data->uid);
                                $point = 100;
                            }
                        }

                        $referralCode = $this->apiCtrl->generate_referralCode();
                        $userInfo = array('CUST_NO '            => $uid,
                                            'CUST_NM'           => $name,
                                            'EMAIL'             => $email,
                                            'COUNTRY_CD'        => $country,
                                            'tier'              => 'Silver',
                                            'usablePoints'      => $point,
                                            'availablePoints'   => $point,
                                            'gainedPoints'      => $point,
                                            'referralCode'      => $referralCode,
                                            'profilePictureInBase64'=> $profilePictureInBase64,
                                            'CUST_SINCE'        => $this->now
                                            );

                        DB::table('customer')->insert($userInfo);

                        if($point == 100){
                            $insertPoint = array('uid'          => $uid,
                                                'insertDate'    => $this->nowDT,
                                                'point'         => 100,
                                                'type'          => 'POINT_ISSUANCE',
                                                'remarks'       => 'Referral',
                                                'isExpired'     => 0
                            );
                            DB::table('point')->insert($insertPoint);
                        }

                        $apiKey = $this->apiCtrl->apiauth();
                        DB::table('CustomerApiAuth')->insert(array('userId'=>$uid,'apikey'=>$apiKey));

                        $this->apiCtrl->_insertEventLogger($this->eventloggerType, $responseName,'New customer account created.',$destination);
                        DB::commit();
                        return Response::json(array(
                            'error'     => false,
                            'code'      => 200,
                            'message'   => 'OK',
                            'data'      => $apiKey
                        ));
                    }else{
                        return Response::json(array(
                            'error'     => true,
                            'code'      => 500,
                            'message'   => 'This account already register.',
                        ));
                    }
                }else{
                    return Response::json(array(
                        'error' => true,
                        'code' => 500,
                        'message' => 'UID not match'
                    ));
                }
            }catch (InvalidToken $e) {
                Log::info($e->getMessage());
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => $e->getMessage()
                ));
            }catch(Exeption $e){
                //eventlog
                DB::rollback();
                $this->apiCtrl->_insertEventLogger($this->eventloggerType, $responseName,$e->getMessage(),$destination);
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => $e->getMessage()
                ));
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            $this->apiCtrl->_insertEventLogger($this->eventloggerType, $responseName,$validationErrorString,$destination);
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }

    public function faceRegister(){
        $requestName = 'FaceRegisterRequest';
        $responseName = 'FaceRegisterResponse';
        $rules = array(
            'apiKey'  => 'required',
            'face'    => 'required'
        );
        $validator = Validator::make(Request::all(), $rules);
        if($validator->passes()){
            $apiKey = Request::get('apiKey');
            $userInfo = $this->apiCtrl->_getCustomerInfo($apiKey);

            if($userInfo){
                $uid = $userInfo->CUST_NO;
                try{
                    DB::beginTransaction();
                    $face = Request::get('face');
                    if(!file_exists('images/customer_face/')) {
                        mkdir('images/customer_face/', 0777, true);
                    }
                    $decode = base64_decode($face);
                    file_put_contents('images/customer_face/'.$uid.'.jpg',$decode);
                    $face_url = $this->imageDefaultHost.'images/customer_face/'.$uid.'.jpg';
                    $newUserFace = array("photo" => $face_url);
                    $check_response = $this->service($this->searchUser_url,'POST',$newUserFace, $requestName.'(CheckUserExist)', $responseName.'(CheckUserExist)');
                    $check_response = json_decode($check_response);

                    if(!empty($check_response)){
                        if(isset($check_response[0]->probability)){
                            if($check_response[0]->probability >= $this->faceProbability){
                                return Response::json(array(
                                    'error' => true,
                                    'code' => 500,
                                    'message' => "This user face is registered"
                                ));
                            }
                        }else{
                            return Response::json(array(
                                'error' => true,
                                'code' => 500,
                                'message' => '(CheckUserFaceExist)'.$check_response
                            ));
                        }
                    }

                    $create_user = array("name" => $uid);
                    $newUser_response = $this->service($this->createPerson_url,'POST', $create_user, $requestName.'(CreatePerson)', $responseName.'(CreatePerson)');
                    $newUser_response = json_decode($newUser_response);
                    if(isset($newUser_response->status)){
                        if($newUser_response->status == 'success'){
                            $personId = $newUser_response->id;
                            $addFace_response = $this->service($this->addFace_url.$personId,'POST', $newUserFace, $requestName.'(AddFace)', $responseName.'(AddFace)');
                            $addFace_response = json_decode($addFace_response);
                            if(isset($addFace_response->status)){
                                if($addFace_response->status == 'success'){
                                    DB::table('customer')->where('CUST_NO', $uid)->update(array('isFaceRegistered' => true, 'faceInBase64' => $face, 'personId' => $personId));
                                    DB::commit();
                                    return Response::json(array(
                                        'error'     => false,
                                        'code'      => 200,
                                        'message'   => 'OK'
                                    ));
                                }
                            }
                            return Response::json(array(
                                'error' => true,
                                'code' => 500,
                                'message' => '(AddFace)'.json_encode($addFace_response)
                            ));
                        }
                    }

                    return Response::json(array(
                        'error' => true,
                        'code' => 500,
                        'message' => '(CreatePerson)'.$newUser_response
                    ));

                }catch(Exeption $e){
                    DB::rollBack();
                    Log::info($e->getMessage());
                    return Response::json(array(
                        'error' => true,
                        'code' => 500,
                        'message' => $e->getMessage()
                    ));
                }
            }else{
                return response('Unauthorized', 401)->header('Content-Type', 'text/plain');
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            Log::info($validationErrorString);
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }

    public function login(){
        $rules = array(
            'idToken'       => 'required',
            'uid'           => 'required'
        );
        $validator = Validator::make(Request::all(), $rules);

        if($validator->passes()){
            $idToken = Request::get('idToken');
            $uid = Request::get('uid');
            try {
                $verifiedIdToken = $this->firebase->getAuth()->verifyIdToken($idToken);
                $firebase_uid = $verifiedIdToken->getClaim('sub');

                if($firebase_uid == $uid){
                    DB::beginTransaction();
                    $data = DB::table('customer')->where('CUST_NO', $uid)->first();
                    DB::commit();

                    if($data){
                        $apiKey = $this->apiCtrl->apiauth();
                        DB::table('CustomerApiAuth')->insert(array('userId'=>$uid,'apikey'=>$apiKey));

                        return Response::json(array(
                            'error'     => false,
                            'code'      => 200,
                            'message'   => 'OK',
                            'data'      => $apiKey
                        ));
                    }else{
                        return Response::json(array(
                            'error'     => true,
                            'code'      => 500,
                            'message'   => 'This account is not registered yet.'
                        ));
                    }
                }else{
                    return Response::json(array(
                        'error' => true,
                        'code' => 500,
                        'message' => 'UID not match'
                    ));
                }
            }catch (InvalidToken $e) {
                Log::info($e->getMessage());
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => $e->getMessage()
                ));
            }catch(Exeption $e){
                DB::rollBack();
                Log::info($e->getMessage());
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => $e->getMessage()
                ));
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            Log::info($validationErrorString);
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }

    public function faceLogin(){
        $requestName = 'FaceLoginRequest';
        $responseName = 'FaceLoginResponse';
        $rules = array(
            'face'       => 'required'
        );
        $validator = Validator::make(Request::all(), $rules);

        if($validator->passes()){
            $face = Request::get('face');
            try {
                DB::beginTransaction();
                $temp_id = $this->apiCtrl->generate_code();
                if(!file_exists('images/customer_face_temp/')) {
                    mkdir('images/customer_face_temp/', 0777, true);
                }
                $decode = base64_decode($face);
                file_put_contents('images/customer_face_temp/'.$temp_id.'.jpg',$decode);
                $face_url = $this->imageDefaultHost.'images/customer_face_temp/'.$temp_id.'.jpg';
                $userFace = array("photo" => $face_url);

                $response = $this->service($this->searchUser_url, 'POST', $userFace, $requestName.'(FaceLogin)', $responseName.'(FaceLogin)');
                $response = json_decode($response);
                if(!empty($response)){
                    if(isset($response[0]->probability)){
                        if($response[0]->probability >= $this->faceProbability){
                            if(isset($response[0]->id)){
                                if(isset($response[0]->name)){
                                    $uid = $response[0]->name;
                                    $personId = $response[0]->id;
                                    $data = DB::table('customer')->where('CUST_NO', $uid)->where('personId', $personId)->first();

                                    File::delete('images/customer_face_temp/'.$temp_id.'.jpg');
                                    DB::commit();
                                    if($data){
                                        $apiKey = $this->apiCtrl->apiauth();
                                        DB::table('CustomerApiAuth')->insert(array('userId'=>$uid,'apikey'=>$apiKey));
                                        return Response::json(array(
                                            'error'     => false,
                                            'code'      => 200,
                                            'message'   => 'OK',
                                            'data'      => $apiKey
                                        ));
                                    }
                                }
                            }
                        }
                    }
                }

                File::delete('images/customer_face_temp/'.$temp_id.'.jpg');
                DB::commit();
                return Response::json(array(
                    'error'     => true,
                    'code'      => 500,
                    'message'   => 'User not found.'
                ));
            }catch(Exeption $e){
                DB::rollBack();
                Log::info($e->getMessage());
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => $e->getMessage()
                ));
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            Log::info($validationErrorString);
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }

    public function removeFaceLogin(){
        $requestName = 'RemoveFaceLoginRequest';
        $responseName = 'RemoveFaceLoginResponse';
        $rules = array(
            'apiKey'  => 'required'
        );
        $validator = Validator::make(Request::all(), $rules);
        if($validator->passes()){
            $apiKey = Request::get('apiKey');
            $userInfo = $this->apiCtrl->_getCustomerInfo($apiKey);

            if($userInfo){
                $uid = $userInfo->CUST_NO;
                $personId = $userInfo->personId;
                try{
                    DB::beginTransaction();
                    File::delete('images/customer_face/'.$uid.'.jpg');
                    $arrayData = array();
                    $response = $this->service($this->addFace_url.$personId, 'DELETE', $arrayData, $requestName, $responseName);
                    $response = json_decode($response);

                    if(isset($response->status)){
                        if($response->status == 'failure'){
                            return Response::json(array(
                                'error' => true,
                                'code' => 500,
                                'message' => $response->message
                            ));
                        }
                    }else{
                        return Response::json(array(
                            'error' => true,
                            'code' => 500,
                            'message' => $response
                        ));
                    }

                    $update = array('isFaceRegistered'=>false, 'faceInBase64'=>null, 'personId'=>null);
                    DB::table('customer')->where('CUST_NO', $uid)->update($update);
                    DB::commit();
                    return Response::json(array(
                        'error' => false,
                        'code' => 200,
                        'message' => 'OK'
                    ));
                }catch(Exeption $e){
                    DB::rollBack();
                    Log::info($e->getMessage());
                    return Response::json(array(
                        'error' => true,
                        'code' => 500,
                        'message' => $e->getMessage()
                    ));
                }
            }else{
                return response('Unauthorized', 401)->header('Content-Type', 'text/plain');
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            Log::info($validationErrorString);
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }


    public function getUserInformation(){
        $rules = array(
            'apiKey'       => 'required'
        );
        $validator = Validator::make(Request::all(), $rules);
        if($validator->passes()){
            $apiKey = Request::get('apiKey');
            $userInfo = $this->apiCtrl->_getCustomerInfo($apiKey);

            if($userInfo){
                $uid = $userInfo->CUST_NO;
                try{
                    $this->checkTier($uid);
                    $data = DB::table('customer')
                            ->select('CUST_NO as uid','profilePictureInBase64','CUST_NM as name','email','PHONE_NO as phoneNumber', 'dob',
                                    'country_cd as country', 'CUST_NO as code','usablePoints','availablePoints', 'gainedPoints', 'tier',
                                    DB::raw("CASE WHEN isFaceRegistered = 1 THEN 'true' ELSE 'false' END AS isFaceRegistered"))
                                    ->where('CUST_NO', $uid)
                                    ->first();
                    $data->usablePoints = (double)$data->usablePoints;
                    $data->availablePoints = (double)$data->availablePoints;

                    $silverTier = DB::table('tier')->select('threshold','reward')->where('name','Silver')->first();
                    $goldTier = DB::table('tier')->select('threshold','reward')->where('name','Gold')->first();
                    $platinumTier = DB::table('tier')->select('threshold','reward')->where('name','Platinum')->first();

                    switch($t = $data->tier){
                        case (strtolower($t) == 'silver'):
                            $nextTier = 'Gold';
                            $silverThreshold = $silverTier->threshold + 1;
                            $percentageToNextTier = $data->gainedPoints / $silverThreshold;
                            $currentTierRewards = $silverTier->reward;
                            $pointsToNextTier =  $silverThreshold - $data->gainedPoints;
                            $nextTierRewards = $goldTier->reward;

                        break;
                        case (strtolower($t) == 'gold'):
                            $nextTier = 'Platinum';
                            $goldThreshold = $goldTier->threshold + 1;
                            $percentageToNextTier = $data->gainedPoints / $goldThreshold;
                            $currentTierRewards = $goldTier->reward;
                            $pointsToNextTier =  $goldThreshold - $data->gainedPoints;
                            $nextTierRewards = $platinumTier->reward;
                        break;
                        case (strtolower($t) == 'platinum'):
                            $nextTier = null;
                            $percentageToNextTier = 1;
                            $currentTierRewards = $platinumTier->reward;
                            $pointsToNextTier =  null;
                            $nextTierRewards = null;
                        break;
                    }

                    $tier = array(  'currentTier'           => $data->tier,
                                    'nextTier'              => $nextTier,
                                    'percentageToNextTier'  => $percentageToNextTier,
                                    'currentTierRewards'    => $currentTierRewards,
                                    'pointsToNextTier'      => $pointsToNextTier,
                                    'nextTierRewards'       => $nextTierRewards
                                    );

                    $data->tier = $tier;
                    unset($data->gainedPoints);
                    return Response::json(array(
                        'error'     => false,
                        'code'      => 200,
                        'message'   => 'OK',
                        'data'      => $data
                    ));
                }catch(Exeption $e){
                    Log::info($e->getMessage());
                    return Response::json(array(
                        'error' => true,
                        'code' => 500,
                        'message' => $e->getMessage()
                    ));
                }
            }else{
                return response('Unauthorized', 401)->header('Content-Type', 'text/plain');
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            Log::info($validationErrorString);
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }

    public function updateUserProfile(){
        $destination = asset('/').Route::getCurrentRoute()->getPath();
        $requestName = "UpdateUserProfileRequest";
        $responseName = "UpdateUserProfileResponse";

        DB::beginTransaction();
        $this->apiCtrl->_insertEventLogger($this->eventloggerType, $requestName, json_encode(Request::all()),$destination);
        DB::commit();

        $rules = array(
            'apiKey'   => 'required'
        );
        $validator = Validator::make(Request::all(), $rules);
        if($validator->passes()){
            $apiKey = Request::get('apiKey');
            $userInfo = $this->apiCtrl->_getCustomerInfo($apiKey);

            if($userInfo){
                $uid = $userInfo->CUST_NO;
                $email = Request::get('email');
                $phoneNumber = Request::get('phoneNumber');
                $name = Request::get('name');
                $country = Request::get('country');
                $dob = Request::get('dob');
                $profilePictureInBase64 = Request::get('profilePictureInBase64');
                $usablePoints = $userInfo->usablePoints;
                $availablePoints = $userInfo->availablePoints;
                $gainedPoints = $userInfo->gainedPoints;
                DB::beginTransaction();
                try{
                    $userInfo = array();
                    if(!empty($name)){
                        $userInfo['CUST_NM'] = $name;
                    }

                    if(!empty($email)){
                        $userInfo['email'] = $email;
                    }

                    if(!empty($phoneNumber)){
                        $hp = DB::table('customer')->where('CUST_NO', $uid)->value('phone_no');
                        if(empty($hp)){
                            $usablePoints += 25;
                            $availablePoints += 25;
                            $gainedPoints += 25;
                            $userInfo['phone_no'] = $phoneNumber;
                            $insertPoint = array('uid'          => $uid,
                                                'insertDate'    => $this->nowDT,
                                                'point'         => 25,
                                                'type'          => 'POINT_ISSUANCE',
                                                'remarks'       => 'Update phone number',
                                                'isExpired'     => 0
                            );
                            DB::table('point')->insert($insertPoint);
                            DB::table('customer')->where('CUST_NO', $uid)->update(array('usablePoints'=>$usablePoints, 'availablePoints'=>$availablePoints, 'gainedPoints'=>$gainedPoints));
                        }else{
                            $userInfo['phone_no'] = $phoneNumber;
                        }
                    }

                    if(!empty($dob)){
                        $db_dob = DB::table('customer')->where('CUST_NO', $uid)->value('dob');
                        if(empty($db_dob)){
                            $usablePoints += 25;
                            $availablePoints += 25;
                            $gainedPoints += 25;
                            $userInfo['dob'] = $dob;
                            $insertPoint = array('uid'          => $uid,
                                                'insertDate'    => $this->nowDT,
                                                'point'         => 25,
                                                'type'          => 'POINT_ISSUANCE',
                                                'remarks'       => 'Update dob',
                                                'isExpired'     => 0
                            );
                            DB::table('point')->insert($insertPoint);
                            DB::table('customer')->where('CUST_NO', $uid)->update(array('usablePoints'=>$usablePoints, 'availablePoints'=>$availablePoints, 'gainedPoints'=>$gainedPoints));
                        }else{
                            $userInfo['dob'] = $dob;
                        }
                    }

                    if(!empty($country)){
                        $userInfo['country_cd'] = $country;
                    }

                    if(!empty($profilePictureInBase64)){
                        $userInfo['profilePictureInBase64'] = $profilePictureInBase64;
                    }

                    DB::table('customer')->where('CUST_NO', $uid)->update($userInfo);
                    $this->checkTier($uid);
                    $this->apiCtrl->_insertEventLogger($this->eventloggerType, $responseName,json_encode($userInfo),$destination);
                    DB::commit();
                    return Response::json(array(
                        'error'     => false,
                        'code'      => 200,
                        'message'   => 'OK'
                    ));
                }catch(Exeption $e){
                    //eventlog
                    DB::rollback();
                    $this->apiCtrl->_insertEventLogger($this->eventloggerType, $responseName,$e->getMessage(),$destination);
                    return Response::json(array(
                        'error' => true,
                        'code' => 500,
                        'message' => $e->getMessage()
                    ));
                }
            }else{
                return response('Unauthorized', 401)->header('Content-Type', 'text/plain');
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            $this->apiCtrl->_insertEventLogger($this->eventloggerType, $responseName,$validationErrorString,$destination);
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }

    public function logout(){
        $rules = array(
            'apiKey'       => 'required'
        );
        $validator = Validator::make(Request::all(), $rules);

        if($validator->passes()){
            $apiKey = Request::get('apiKey');

            try{
                DB::table('CustomerApiAuth')->where('apiKey', $apiKey)->delete();

                return Response::json(array(
                    'error'     => false,
                    'code'      => 200,
                    'message'   => 'OK'
                ));
            }catch(Exeption $e){
                DB::rollback();
                Log::info($e->getMessage());
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => $e->getMessage()
                ));
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            Log::info($validationErrorString);
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }

    public function getInvitationInfo(){
        $rules = array(
            'apiKey'       => 'required'
        );
        $validator = Validator::make(Request::all(), $rules);

        if($validator->passes()){
            $apiKey = Request::get('apiKey');
            $userInfo = $this->apiCtrl->_getCustomerInfo($apiKey);

            if($userInfo){
                $referralCode = $userInfo->referralCode;

                try{
                /*  $dynamicLink = new yedincisenol\DynamicLinks\DynamicLinks([
                        'api_key'               =>  'AIzaSyCZmsmdPNMBJGULAkFJ56zyyiwuzBpU0qo',
                        'dynamic_link_domain'   =>  'easierm.page.link'
                    ]); */

                    $data = array();
                    // $referralCode = DB::table('customer')->where('CUST_NO', $uid)->value('referralCode');
                    /* $link = new yedincisenol\DynamicLinks\DynamicLink('http://yeni.co/');
                    $shortLink = $dynamicLink->create($link, 'UNGUESSABLE'); */

                    $data['referralCode'] = $referralCode;
                    $data['shareMessage'] = 'Invite each friend to earn 100 points.';
                    /* foreach($data as $d){
                        $d->referralCode = $referralCode;
                        $d->dynamicLink = $shortLink;
                        $d->shareMessage = 'Invite each friend to earn 100 points.';
                    } */

                    return Response::json(array(
                        'error'     => false,
                        'code'      => 200,
                        'message'   => 'OK',
                        'data'      => $data
                    ));
                }catch(Exeption $e){
                    Log::info($e);
                    return Response::json(array(
                        'error' => true,
                        'code' => 500,
                        'message' => $e->getMessage()
                    ));
                }
            }else{
                return response('Unauthorized', 401)->header('Content-Type', 'text/plain');
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            Log::info($validationErrorString);
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }
}
