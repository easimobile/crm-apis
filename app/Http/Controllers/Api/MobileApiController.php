<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController as ApiController;
use view;
use Input;
use Request;
use Validator;
use Response;
use Log;
use DB;
use Route;
// use Config;

class MobileApiController{
	public function __construct(){
        $this->apiCtrl = new ApiController();
        $this->notificationApiKey = "NzNlNDRkYTctNWIxNy00YzlkLWJiZGEtZTYwNjQ2NjFjZDFl";
        $this->notificationAppID = "d71ef435-3a96-4ad4-84a3-1c4ca52450c3";
        $this->page = 10;
        $this->point = 10;
        $this->now = date("Y-m-d H:i:s");
        /* $str_length = 6;
        $newRcpNo = substr("000000{$rcpNo}", -$str_length); */
    }

    public function getEvents(){
        $rules = array(
            'apiKey'       => 'required'
        );
        $validator = Validator::make(Request::all(), $rules);
        if($validator->passes()){
            $apiKey = Request::get('apiKey');
            try{
                DB::beginTransaction();
                $userInfo = $this->apiCtrl->_getCustomerInfo($apiKey);

                if($userInfo){
                    $data = DB::table('events')
                            ->select('eventId','eventName','eventShortDescription','imageInBase64','type')
                            ->where('isActive', 1)
                            ->where('endDate', '>=', date('Y-m-d H:i:s'))
                            ->where('startDate', '<=', date('Y-m-d H:i:s'))
                            ->orderby('startDate', 'asc')
                            ->get();

                    foreach($data as $d){
                        $typeInFullName = DB::table('types')->where('type',$d->type)->where('category','EVENT')->value('typeInFullName');
                        if($typeInFullName){
                            $d->typeInFullName = $typeInFullName;
                        }
                    }

                    DB::commit();
                    return Response::json(array(
                        'error'     => false,
                        'code'      => 200,
                        'message'   => 'OK',
                        'data'      => $data
                    ));
                }else{
                    return response('Unauthorized', 401)->header('Content-Type', 'text/plain');
                }

            }catch(Exeption $e){
                Log::info($e);
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => $e->getMessage()
                ));
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            Log::info($validationErrorString);
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }

    public function getEventDetails(){
        $rules = array(
            'apiKey'       => 'required',
            'eventId'      => 'required|exists:events,eventId'
        );
        $validator = Validator::make(Request::all(), $rules);
        if($validator->passes()){
            $eventId = Request::get('eventId');
            $apiKey = Request::get('apiKey');

            try{
                DB::beginTransaction();
                $userInfo = $this->apiCtrl->_getCustomerInfo($apiKey);

                if($userInfo){
                    $data = DB::table('events')
                            ->select('eventId','eventName','eventLongDescription','eventTnc', 'imageInBase64','startDate','endDate','createdAt','type','contactName','contactPhoneNumber','contactEmail')
                            ->where('eventId',$eventId)
                            ->first();

                    $typeInFullName = DB::table('types')->where('type',$data->type)->where('category','EVENT')->value('typeInFullName');
                    if($typeInFullName){
                        $data->typeInFullName = $typeInFullName;
                    }

                    DB::commit();
                    return Response::json(array(
                        'error'     => false,
                        'code'      => 200,
                        'message'   => 'OK',
                        'data'      => $data
                    ));
                }else{
                    return response('Unauthorized', 401)->header('Content-Type', 'text/plain');
                }

            }catch(Exeption $e){
                Log::info($e);
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => $e->getMessage()
                ));
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            Log::info($validationErrorString);
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }

    public function getRewards(){
        $rules = array(
            'apiKey'       => 'required'
        );
        $validator = Validator::make(Request::all(), $rules);
        if($validator->passes()){
            $apiKey = Request::get('apiKey');
            try{
                DB::beginTransaction();
                $userInfo = $this->apiCtrl->_getCustomerInfo($apiKey);

                if($userInfo){
                    $data = DB::table('reward')
                            ->select('rewardId','rewardName','rewardShortDescription','imageInBase64','type','pointsToRedeem')
                            ->where('isActive', 1)
                            ->where('endDate', '>=', date('Y-m-d H:i:s'))
                            ->where('startDate', '<=', date('Y-m-d H:i:s'))
                            ->orderby('startDate', 'asc')
                            ->get();

                    foreach($data as $d){
                        $typeInFullName = DB::table('types')->where('type',$d->type)->where('category','REWARD')->value('typeInFullName');
                        if($typeInFullName){
                            $d->typeInFullName = $typeInFullName;
                        }
                    }
                    DB::commit();
                    return Response::json(array(
                        'error'     => false,
                        'code'      => 200,
                        'message'   => 'OK',
                        'data'      => $data
                    ));
                }else{
                    return response('Unauthorized', 401)->header('Content-Type', 'text/plain');
                }

            }catch(Exeption $e){
                Log::info($e);
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => $e->getMessage()
                ));
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            Log::info($validationErrorString);
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }

    public function getRewardDetails(){
        $rules = array(
            'apiKey'       => 'required',
            'rewardId'      => 'required|exists:reward,rewardId'
        );
        $validator = Validator::make(Request::all(), $rules);
        if($validator->passes()){
            $rewardId = Request::get('rewardId');
            $apiKey = Request::get('apiKey');

            try{
                DB::beginTransaction();
                $userInfo = $this->apiCtrl->_getCustomerInfo($apiKey);

                if($userInfo){
                    $uid = $userInfo->CUST_NO;
                    $data = DB::table('reward as r')
                            ->leftjoin('voucher as v', 'v.voucherId','=','r.voucherId')
                            ->select('r.rewardId','r.rewardName','r.rewardLongDescription','r.rewardTnc', 'r.imageInBase64','r.startDate','r.endDate','r.maxRedeemedPerReward','r.maxRedeemedPerUser','r.createdAt','r.location','r.type','v.pointsToRedeem')
                            ->where('r.rewardId',$rewardId)
                            ->first();

                    $typeInFullName = DB::table('types')->where('type',$data->type)->where('category','REWARD')->value('typeInFullName');
                    if($typeInFullName){
                        $data->typeInFullName = $typeInFullName;
                    }

                    $data->pointsToRedeem = (double)$data->pointsToRedeem;
                    $point = $userInfo->usablePoints;
                    $pointsToRedeem = $data->pointsToRedeem;
                    $maxRedeemedPerReward = $data->maxRedeemedPerReward;
                    $maxRedeemedPerUser = $data->maxRedeemedPerUser;

                    $existReward =  DB::table('UserReward')->where('uid',$uid)->where('rewardId',$rewardId)->first();
                    if($existReward){
                        $rewardRedeemed = DB::table('UserReward')->where('rewardId',$rewardId)->get();
                        $userRedeemed =  DB::table('UserReward')->where('uid',$uid)->where('rewardId',$rewardId)->get();
                        if((count($rewardRedeemed) < $maxRedeemedPerReward) || $maxRedeemedPerReward == -1 ){
                            if((count($userRedeemed) < $maxRedeemedPerUser) || $maxRedeemedPerUser == -1 ){
                                if($point >= $pointsToRedeem){
                                    $data->isRedeemable = 'true';
                                    $data->status = 'NONE';
                                }else{
                                    $data->isRedeemable = 'false';
                                    $data->status = 'INSUFFICIENT_POINT';
                                }
                            }else{
                                $data->isRedeemable = 'false';
                                $data->status = 'MAX_REDEEMED_USER';
                            }
                        }else{
                            $data->isRedeemable = 'false';
                            $data->status = 'MAX_REDEEMED_REWARD';
                        }
                    }else{
                        if($point >= $pointsToRedeem){
                            $data->isRedeemable = 'true';
                            $data->status = 'NONE';
                        }else{
                            $data->isRedeemable = 'false';
                            $data->status = 'INSUFFICIENT_POINT';
                        }
                    }

                    unset($data->maxRedeemedPerReward);
                    unset($data->maxRedeemedPerUser);
                    DB::commit();
                    return Response::json(array(
                        'error'     => false,
                        'code'      => 200,
                        'message'   => 'OK',
                        'data'      => $data
                    ));
                }else{
                    return response('Unauthorized', 401)->header('Content-Type', 'text/plain');
                }

            }catch(Exeption $e){
                Log::info($e);
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => $e->getMessage()
                ));
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            Log::info($validationErrorString);
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }

    public function getUserActiveRewards(){
        $rules = array(
            'apiKey'       => 'required',
            'page'         => 'required'
        );
        $validator = Validator::make(Request::all(), $rules);
        if($validator->passes()){
            $apiKey = Request::get('apiKey');
            $page = Request::get('page');

            try{
                DB::beginTransaction();
                $userInfo = $this->apiCtrl->_getCustomerInfo($apiKey);

                if($userInfo){
                    $uid = $userInfo->CUST_NO;
                    $reward = DB::table('UserReward as ur')
                            ->leftjoin('Reward as r', 'r.rewardId','=','ur.rewardId')
                            ->leftjoin('voucher as v', 'v.voucherId','=','r.voucherId')
                            ->select('ur.id','ur.rewardId','r.rewardName','r.imageInBase64','r.startDate','r.endDate',
                                    DB::raw("CASE WHEN ur.isClaimed = 1 THEN 'true' ELSE 'false' END AS isClaimed"))
                            ->whereDate('v.expiredAt', '>=', date('Y-m-d'))
                            ->where('ur.uid',$uid)
                            ->where('ur.isClaimed', false)
                            ->get();

                    if(count($reward) <= 10){
                        if($page == 1){
                            $data = array();
                            $data['page'] = $page;
                            $data['hasNext'] = 'false';
                            $data['total'] = count($reward);
                            $data['list'] = $reward;
                        }else{
                            $list = array();
                            $data = array(  'page'      => $page,
                                            'hasNext'   => 'false',
                                            'list'      => $list
                            );
                        }
                    }else{
                        $array = json_decode(json_encode($reward), true);
                        $chunk_items = array_chunk($array, $this->page);
                        $count_page = count($chunk_items);

                        if($page <= $count_page){
                            $data = array();
                            for($i = 1; $i <= $page; $i++){
                                $data['page'] = $page;
                                if($i === $count_page) {
                                    $data['hasNext'] = 'false';
                                }else{
                                    $data['hasNext'] = 'true';
                                }
                                $data['total'] = count($reward);
                                $s = $i - 1;
                                $data['list'] = $chunk_items[$s];
                            }
                        }else{
                            $list = array();
                            $data = array('page'        => $page,
                                            'hasNext'   => 'false',
                                            'list'      => $list
                            );
                        }
                    }
                    DB::commit();
                    return Response::json(array(
                        'error'     => false,
                        'code'      => 200,
                        'message'   => 'OK',
                        'data'      => $data
                    ));
                }else{
                    return response('Unauthorized', 401)->header('Content-Type', 'text/plain');
                }

            }catch(Exeption $e){
                Log::info($e);
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => $e->getMessage()
                ));
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            Log::info($validationErrorString);
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }

    public function getUserPastRewards(){
        $rules = array(
            'apiKey'       => 'required',
            'page'         => 'required'
        );
        $validator = Validator::make(Request::all(), $rules);
        if($validator->passes()){
            $apiKey = Request::get('apiKey');
            $page = Request::get('page');

            try{
                DB::beginTransaction();
                $userInfo = $this->apiCtrl->_getCustomerInfo($apiKey);

                if($userInfo){
                    $uid = $userInfo->CUST_NO;
                    $reward = DB::table('UserReward as ur')
                            ->leftjoin('Reward as r', 'r.rewardId','=','ur.rewardId')
                            ->leftjoin('voucher as v', 'v.voucherId','=','r.voucherId')
                            ->select('ur.id','ur.rewardId','r.rewardName','r.imageInBase64')
                            ->whereDate('v.expiredAt', '<', date('Y-m-d'))
                            ->where('ur.uid',$uid)
                            ->get();

                    if(count($reward) <= 10){
                        if($page == 1){
                            $data = array();
                            $data['page'] = $page;
                            $data['hasNext'] = 'false';
                            $data['total'] = count($reward);
                            $data['list'] = $reward;
                        }else{
                            $list = array();
                            $data = array(  'page'      => $page,
                                            'hasNext'   => 'false',
                                            'list'      => $list
                            );
                        }
                    }else{
                        $array = json_decode(json_encode($reward), true);
                        $chunk_items = array_chunk($array, $this->page);
                        $count_page = count($chunk_items);

                        if($page <= $count_page){
                            $data = array();
                            for($i = 1; $i <= $page; $i++){
                                $data['page'] = $page;
                                if($i === $count_page) {
                                    $data['hasNext'] = 'false';
                                }else{
                                    $data['hasNext'] = 'true';
                                }
                                $data['total'] = count($reward);
                                $s = $i - 1;
                                $data['list'] = $chunk_items[$s];
                            }
                        }else{
                            $list = array();
                            $data = array('page'        => $page,
                                            'hasNext'   => 'false',
                                            'list'      => $list
                            );
                        }
                    }
                    DB::commit();
                    return Response::json(array(
                        'error'     => false,
                        'code'      => 200,
                        'message'   => 'OK',
                        'data'      => $data
                    ));
                }else{
                    return response('Unauthorized', 401)->header('Content-Type', 'text/plain');
                }

            }catch(Exeption $e){
                Log::info($e);
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => $e->getMessage()
                ));
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            Log::info($validationErrorString);
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }

    public function getUserRewardDetails(){
        $rules = array(
            'apiKey'  => 'required',
            'id'      => 'required|exists:UserReward,id'
        );
        $validator = Validator::make(Request::all(), $rules);
        if($validator->passes()){
            $apiKey = Request::get('apiKey');
            $id = Request::get('id');

            try{
                DB::beginTransaction();
                $userInfo = $this->apiCtrl->_getCustomerInfo($apiKey);

                if($userInfo){
                    $uid = $userInfo->CUST_NO;
                    $data = DB::table('UserReward as ur')
                            ->leftjoin('Reward as r', 'r.rewardId','=','ur.rewardId')
                            ->leftjoin('voucher as v', 'v.voucherId','=','r.voucherId')
                            ->select('ur.rewardId','v.voucherCode','r.rewardName','r.rewardLongDescription','r.rewardTnc','r.imageInBase64','r.startDate','r.endDate','v.createdAt','r.location')
                            ->whereDate('v.expiredAt', '>=', date('Y-m-d'))
                            ->where('ur.uid',$uid)
                            ->where('ur.id',$id)
                            ->where('ur.isClaimed', false)
                            ->first();

                    DB::commit();
                    return Response::json(array(
                        'error'     => false,
                        'code'      => 200,
                        'message'   => 'OK',
                        'data'      => $data
                    ));
                }else{
                    return response('Unauthorized', 401)->header('Content-Type', 'text/plain');
                }

            }catch(Exeption $e){
                Log::info($e);
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => $e->getMessage()
                ));
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            Log::info($validationErrorString);
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }

    public function redeemReward(){
        $rules = array(
            'apiKey'       => 'required',
            'rewardId'      => 'required|exists:reward,rewardId'
        );
        $validator = Validator::make(Request::all(), $rules);
        if($validator->passes()){
            $rewardId = Request::get('rewardId');
            $apiKey = Request::get('apiKey');

            try{
                DB::beginTransaction();
                $userInfo = $this->apiCtrl->_getCustomerInfo($apiKey);

                if($userInfo){
                    $uid = $userInfo->CUST_NO;
                    $usablePoints = $userInfo->usablePoints;
                    $availablePoints = $userInfo->availablePoints;

                    $active = DB::table('reward')->where('rewardId',$rewardId)->value('isActive');
                    if($active == 1){
                        $reward = DB::table('reward')
                                ->select('rewardId','rewardName','rewardLongDescription','rewardTnc', 'imageInBase64','startDate','endDate','maxRedeemedPerReward','maxRedeemedPerUser','createdAt','location','type','pointsToRedeem')
                                ->where('rewardId',$rewardId)
                                ->first();

                        $typeInFullName = DB::table('types')->where('type',$reward->type)->where('category','REWARD')->value('typeInFullName');
                        if($typeInFullName){
                            $reward->typeInFullName = $typeInFullName;
                        }

                        $pointsToRedeem = $reward->pointsToRedeem;
                        $maxRedeemedPerReward = $reward->maxRedeemedPerReward;
                        $maxRedeemedPerUser = $reward->maxRedeemedPerUser;

                        $rewardRedeemed = DB::table('UserReward')->where('rewardId',$rewardId)->get();
                        $userRedeemed =  DB::table('UserReward')->where('uid',$uid)->where('rewardId',$rewardId)->get();
                        if((count($rewardRedeemed) < $maxRedeemedPerReward) || $maxRedeemedPerReward == -1 ){
                            if(count($userRedeemed) >= $maxRedeemedPerUser && $maxRedeemedPerUser != -1 ){
                                return Response::json(array(
                                    'error'     => false,
                                    'code'      => 200,
                                    'message'   => 'MAX_REDEEMED_USER'
                                ));
                            }
                        }else{
                            return Response::json(array(
                                'error'     => false,
                                'code'      => 200,
                                'message'   => 'MAX_REDEEMED_REWARD'
                            ));
                        }

                        if($usablePoints >= $pointsToRedeem){
                            $usablePoints -= $pointsToRedeem;
                            $availablePoints -= $pointsToRedeem;

                            $latestVoucher = DB::table('voucher')->orderby('voucherCode','desc')->value('voucherCode');

                            if($latestVoucher){
                                $voucherNo = substr($latestVoucher, 2);
                                $voucherNo += 1;
                                $voucherNo = substr("0000000000{$voucherNo}", -10);
                                $voucher = 'VC'.$voucherNo;
                            }else{
                                $voucher = 'VC0000000001';
                            }

                            $voucherArray = array('rewardId'        => $rewardId,
                                                'voucherCode'     => $voucher,
                                                'pointsToRedeem'  => $pointsToRedeem,
                                                'createdAt'       => $this->now
                                                );

                            $voucherId = DB::table('voucher')->insertGetId($voucherArray);
                            DB::table('customer')->where('Cust_no',$uid)->update(array('usablePoints' => $usablePoints, 'availablePoints' => $availablePoints));
                            DB::table('Point')->insert(array('uid' => $uid, 'insertDate' => $this->now, 'point' => '-'.$pointsToRedeem, 'type' => 'POINT_DEDUCTION', 'remarks' => 'Redeem reward', 'rewardId' => $rewardId));
                            DB::table('UserReward')->insert(array('uid' => $uid, 'rewardId' => $rewardId, 'voucherId' => $voucherId, 'isClaimed'=> false, 'createdAt' => $this->now));

                            $messageContent = DB::table('notificationMessage')->where('category','REWARD_REDEMPTION')->first();
                            $title = $messageContent->title;
                            $body = $messageContent->body;
                            if(strpos($body, '[REWARDNAME]') !== false) {
                                $body = str_replace("[REWARDNAME]",$reward->rewardName,$body);
                            }
                            if(strpos($body, '[POINT]') !== false) {
                                $body = str_replace("[POINT]",$availablePoints,$body);
                            }

                            $this->_sendNotification($title,$body,'REWARD_REDEMPTION',$uid);
                        }else{
                            return Response::json(array(
                                'error'     => false,
                                'code'      => 200,
                                'message'   => 'INSUFFICIENT_POINT'
                            ));
                        }
                        DB::commit();
                        return Response::json(array(
                            'error'     => false,
                            'code'      => 200,
                            'message'   => 'OK'
                        ));
                    }else{
                        return Response::json(array(
                            'error'     => false,
                            'code'      => 200,
                            'message'   => 'INACTIVE_REWARD'
                        ));
                    }
                }else{
                    return response('Unauthorized', 401)->header('Content-Type', 'text/plain');
                }
            }catch(Exeption $e){
                DB::rollback();
                Log::info($e);
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => $e->getMessage()
                ));
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            Log::info($validationErrorString);
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }

    public function claimMyReward(){
        $rules = array(
            'apiKey'       => 'required',
            'rewardId'      => 'required|exists:reward,rewardId'
        );
        $validator = Validator::make(Request::all(), $rules);
        if($validator->passes()){
            $rewardId = Request::get('rewardId');
            $apiKey = Request::get('apiKey');

            try{
                DB::beginTransaction();
                $userInfo = $this->apiCtrl->_getCustomerInfo($apiKey);

                if($userInfo){
                    $uid = $userInfo->CUST_NO;
                    $reward = DB::table('UserReward')
                            ->where('uid', $uid)
                            ->where('rewardId', $rewardId)
                            ->where('isClaimed', false)
                            ->get();
                    if($reward){
                        $reward_quantity = count($reward);
                        if($reward_quantity > 0){
                            $id = DB::table('UserReward')
                                ->where('uid', $uid)
                                ->where('rewardId', $rewardId)
                                ->where('isClaimed', false)
                                ->value('id');

                            DB::table('UserReward')
                            ->where('uid', $uid)
                            ->where('id', $id)
                            ->update(array('isClaimed' => true, 'claimDate' => date('Y-m-d H:i:s')));

                            //Send Notification
                            $rewardName = DB::table('Reward')->where('rewardId',$reward->rewardId)->value('rewardName');
                            $messageContent = DB::table('notificationMessage')->where('category','REWARD_CLAIM')->first();
                            $title = $messageContent->title;
                            $body = $messageContent->body;
                            if(strpos($body, '[REWARD_NAME]') !== false) {
                                $body = str_replace("[REWARD_NAME]",$rewardName,$body);
                            }
                            $this->_sendNotification($title,$body,'REWARD_CLAIM',$uid);
                        }else{
                            return Response::json(array(
                                'error'     => false,
                                'code'      => 200,
                                'message'   => 'MAX_CLAIMED_REWARD'
                            ));
                        }
                    }else{
                        return Response::json(array(
                            'error'     => false,
                            'code'      => 200,
                            'message'   => 'Reward not exist'
                        ));
                    }
                    DB::commit();
                    return Response::json(array(
                        'error'     => false,
                        'code'      => 200,
                        'message'   => 'OK'
                    ));
                }else{
                    return response('Unauthorized', 401)->header('Content-Type', 'text/plain');
                }
            }catch(Exeption $e){
                DB::rollback();
                Log::info($e);
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => $e->getMessage()
                ));
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            Log::info($validationErrorString);
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }

    public function getTransactionHistory(){
        $rules = array(
            'apiKey'       => 'required',
            'page'         => 'required'
        );
        $validator = Validator::make(Request::all(), $rules);

        if($validator->passes()){

            try{
                DB::beginTransaction();
                $apiKey = Request::get('apiKey');
                $userInfo = $this->apiCtrl->_getCustomerInfo($apiKey);

                if($userInfo){
                    $uid = $userInfo->CUST_NO;
                    $transaction = DB::table('transactions')->where('uid',$uid)->orderby('transactionDate','desc')->get();
                    $page = Request::get('page');

                    foreach($transaction as $t){
                        $t->totalAmount = (double)$t->totalAmount;
                    }

                    if(count($transaction) <= 10){
                        if($page == 1){
                            $data = array();
                            $data['page'] = $page;
                            $data['hasNext'] = 'false';
                            $data['total'] = count($transaction);
                            $data['list'] = $transaction;
                        }else{
                            $list = array();
                            $data = array('page'        => $page,
                                            'hasNext'   => 'false',
                                            'list'      => $list
                            );
                        }
                    }else{
                        $array = json_decode(json_encode($transaction), true);
                        $chunk_items = array_chunk($array, $this->page);
                        $count_page = count($chunk_items);

                        if($page <= $count_page){
                            $data = array();
                            for($i = 1; $i <= $page; $i++){
                                $data['page'] = $page;
                                if($i === $count_page) {
                                    $data['hasNext'] = 'false';
                                }else{
                                    $data['hasNext'] = 'true';
                                }
                                $data['total'] = count($transaction);
                                $s = $i - 1;
                                $data['list'] = $chunk_items[$s];
                            }
                        }else{
                            $list = array();
                            $data = array('page'        => $page,
                                            'hasNext'   => 'false',
                                            'list'      => $list
                            );
                        }
                    }
                    DB::commit();
                    return Response::json(array(
                        'error'     => false,
                        'code'      => 200,
                        'message'   => 'OK',
                        'data'      => $data
                    ));
                }else{
                    return response('Unauthorized', 401)->header('Content-Type', 'text/plain');
                }

            }catch(Exeption $e){
                Log::info($e);
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => $e->getMessage()
                ));
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            Log::info($validationErrorString);
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }

    public function getPointHistory(){
        $rules = array(
            'apiKey'       => 'required',
            'page'         => 'required'
        );
        $validator = Validator::make(Request::all(), $rules);

        if($validator->passes()){

            try{
                DB::beginTransaction();
                $apiKey = Request::get('apiKey');
                $userInfo = $this->apiCtrl->_getCustomerInfo($apiKey);
                if($userInfo){
                    $uid = $userInfo->CUST_NO;
                    $page = Request::get('page');
                    $point = DB::table('point')->where('uid',$uid)->orderby('insertDate','desc')->get();

                    foreach($point as $p){
                        $p->point = (double)$p->point;
                        $p->rewardName = DB::table('reward')->where('rewardId',$p->rewardId)->value('rewardName');
                        unset($p->rewardId);
                    }

                    if(count($point) <= 10){
                        if($page == 1){
                            $data = array();
                            $data['page'] = $page;
                            $data['hasNext'] = 'false';
                            $data['total'] = count($point);
                            $data['list'] = $point;
                        }else{
                            $list = array();
                            $data = array('page'        => $page,
                                            'hasNext'   => 'false',
                                            'list'      => $list
                            );
                        }
                    }else{
                        $array = json_decode(json_encode($point), true);
                        $chunk_items = array_chunk($array, $this->page);
                        $count_page = count($chunk_items);

                        if($page <= $count_page){
                            $data = array();
                            for($i = 1; $i <= $page; $i++){
                                $data['page'] = $page;
                                if($i === $count_page) {
                                    $data['hasNext'] = 'false';
                                }else{
                                    $data['hasNext'] = 'true';
                                }
                                $data['total'] = count($point);
                                $s = $i - 1;
                                $data['list'] = $chunk_items[$s];
                            }
                        }else{
                            $list = array();
                            $data = array('page'        => $page,
                                            'hasNext'   => 'false',
                                            'list'      => $list
                            );
                        }
                    }

                    DB::commit();
                    return Response::json(array(
                        'error'     => false,
                        'code'      => 200,
                        'message'   => 'OK',
                        'data'      => $data
                    ));
                }else{
                    return response('Unauthorized', 401)->header('Content-Type', 'text/plain');
                }

            }catch(Exeption $e){
                Log::info($e);
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => $e->getMessage()
                ));
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            Log::info($validationErrorString);
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }

    public function notification($json){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://onesignal.com/api/v1/notifications",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $json,
            CURLOPT_HTTPHEADER => array("Content-Type: application/json",
                                        "Authorization: Basic ".$this->notificationApiKey
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }

    public function sendNotification(){
        $rules = array(
            'title'       => 'required',
            'body'         => 'required',
            'category'       => 'required'
        );
        $validator = Validator::make(Request::all(), $rules);
        if($validator->passes()){
            try{
                DB::beginTransaction();
                $title = Request::get('title');
                $body = Request::get('body');
                $uids = Request::get('uids');
                $sendAfter = Request::get('sendAfter');
                $category = Request::get('category');

                $json = array("app_id" => $this->notificationAppID, "mutable_content" => true);
                if(empty($uids)){
                    $json['included_segments'] = "Subscribed Users";
                }else{
                    $token = array();
                    foreach($uids as $id){
                        $pt_id = DB::table("PushToken")->where("uid",$id)->pluck("pushToken")->toArray();
                        $token = array_merge($token, $pt_id);
                    }
                    $json['include_player_ids'] = $token;
                }
                $json['headings'] = array("en"=> $title);
                $json['contents'] = array("en"=> $body);
                $json['data'] = array("pn_title"=> $title, "pn_body"=> $body, "pn_category"=> $category);
                if(!empty($send_after)){
                    $json['send_after'] = $sendAfter;
                }
                $json = json_encode($json);
                $response = $this->notification($json);
                $response = json_decode($response,true);

                if(empty($uids)){
                    $uids = DB::table("Customer")->pluck("CUST_NO");
                }

                foreach($uids as $id){
                    $insertArray = array("uid"          => $id,
                                        "createdAt"     => $this->now,
                                        "title"         => $title,
                                        "body"          => $body,
                                        "isRead"        => false,
                                        "isDismissed"   => false,
                                        "isNew"         => false,
                                        "category"      => $category
                                        );

                    DB::table('Notifications')->insert($insertArray);
                }

                if(isset($response['errors'])){
                    $destination = asset('/').Route::getCurrentRoute()->getPath();
                    $this->apiCtrl->_insertEventLogger('API(MobileApp)', 'sendNotification',$response['errors'][0],$destination);
                }

                DB::commit();
                return Response::json(array(
                    'error'     => false,
                    'code'      => 200,
                    'message'   => 'OK'
                ));

            }catch(Exeption $e){
                DB::rollback();
                Log::info($e);
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => $e->getMessage()
                ));
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            Log::info($validationErrorString);
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }

    public function _sendNotification($title,$body,$category,$uid){
        try{
            DB::beginTransaction();
            $pt_id = DB::table("PushToken")->where("uid",$uid)->pluck("pushToken")->toArray();
            if(!empty($pt_id)){
                $json = array("app_id" => $this->notificationAppID, "mutable_content" => true);
                $json['include_player_ids'] = $pt_id;
                $json['headings'] = array("en"=> $title);
                $json['contents'] = array("en"=> $body);
                $json['data'] = array("pn_title"=> $title, "pn_body"=> $body, "pn_category"=> $category);
                $json = json_encode($json);

                $response = $this->notification($json);
                $response = json_decode($response,true);

                $insertArray = array("uid"          => $uid,
                                    "createdAt"     => $this->now,
                                    "title"         => $title,
                                    "body"          => $body,
                                    "isRead"        => false,
                                    "isDismissed"   => false,
                                    "isNew"         => false,
                                    "category"      => $category
                                    );

                DB::table('Notifications')->insert($insertArray);

                if(isset($response['errors'])){
                    $destination = asset('/').Route::getCurrentRoute()->getPath();
                    $this->apiCtrl->_insertEventLogger('API', '_sendNotification',$response['errors'][0],$destination);
                }
            }
            DB::commit();
            return;
        }catch(Exeption $e){
            DB::rollback();
            Log::info($e);
            return Response::json(array(
                'error' => true,
                'code' => 500,
                'message' => $e->getMessage()
            ));
        }
    }

    public function registerPushToken(){
        $rules = array(
            'apiKey'    => 'required',
            'pushToken' => 'required',
            'os'        => 'required'
        );
        $validator = Validator::make(Request::all(), $rules);
        if($validator->passes()){
            $apiKey = Request::get('apiKey');
            $pushToken = Request::get('pushToken');
            $os = Request::get('os');

            try{
                DB::beginTransaction();
                $userInfo = $this->apiCtrl->_getCustomerInfo($apiKey);

                if($userInfo){
                    $uid = $userInfo->CUST_NO;
                    DB::table('PushToken')->insert(array("uid"=>$uid, "pushToken"=>$pushToken, "os"=>$os));

                    DB::commit();
                    return Response::json(array(
                        'error'     => false,
                        'code'      => 200,
                        'message'   => 'OK'
                    ));
                }else{
                    return response('Unauthorized', 401)->header('Content-Type', 'text/plain');
                }
            }catch(Exeption $e){
                DB::rollback();
                Log::info($e);
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => $e->getMessage()
                ));
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            Log::info($validationErrorString);
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }

    public function removePushToken(){
        $rules = array(
            'apiKey'    => 'required',
            'pushToken' => 'required|exists:PushToken,pushToken'
        );
        $validator = Validator::make(Request::all(), $rules);
        if($validator->passes()){
            $apiKey = Request::get('apiKey');
            $pushToken = Request::get('pushToken');

            try{
                DB::beginTransaction();
                $userInfo = $this->apiCtrl->_getCustomerInfo($apiKey);
                if($userInfo){
                    $uid = $userInfo->CUST_NO;
                    DB::table('PushToken')->where("uid",$uid)->where("pushToken",$pushToken)->delete();

                    DB::commit();
                    return Response::json(array(
                        'error'     => false,
                        'code'      => 200,
                        'message'   => 'OK'
                    ));
                }else{
                    return response('Unauthorized', 401)->header('Content-Type', 'text/plain');
                }
            }catch(Exeption $e){
                DB::rollback();
                Log::info($e);
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => $e->getMessage()
                ));
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            Log::info($validationErrorString);
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }

    public function getNotification(){
        $rules = array(
            'apiKey'       => 'required',
            'page'         => 'required'
        );
        $validator = Validator::make(Request::all(), $rules);
        if($validator->passes()){
            try{
                DB::beginTransaction();
                $apiKey = Request::get('apiKey');
                $userInfo = $this->apiCtrl->_getCustomerInfo($apiKey);

                if($userInfo){
                    $uid = $userInfo->CUST_NO;
                    $notification = DB::table('notifications')
                                    ->select('*', DB::raw("CASE WHEN isRead = 1 THEN 'true' ELSE 'false' END AS isRead"),
                                            DB::raw("CASE WHEN isDismissed = 1 THEN 'true' ELSE 'false' END AS isDismissed"),
                                            DB::raw("CASE WHEN isNew = 1 THEN 'true' ELSE 'false' END AS isNew"))
                                    ->where('uid', $uid)
                                    ->where('isDismissed', false)
                                    ->orderby('createdAt','desc')
                                    ->get();
                    $page = Request::get('page');

                    if(count($notification) <= 10){
                        if($page == 1){
                            $data = array();
                            $data['page'] = $page;
                            $data['hasNext'] = 'false';
                            $data['total'] = count($notification);
                            $data['list'] = $notification;
                        }else{
                            $list = array();
                            $data = array('page'        => $page,
                                            'hasNext'   => 'false',
                                            'list'      => $list
                            );
                        }
                    }else{
                        $array = json_decode(json_encode($notification), true);
                        $chunk_items = array_chunk($array, $this->page);
                        $count_page = count($chunk_items);

                        if($page <= $count_page){
                            $data = array();
                            for($i = 1; $i <= $page; $i++){
                                $data['page'] = $page;
                                if($i === $count_page) {
                                    $data['hasNext'] = 'false';
                                }else{
                                    $data['hasNext'] = 'true';
                                }
                                $data['total'] = count($notification);
                                $s = $i - 1;
                                $data['list'] = $chunk_items[$s];
                            }
                        }else{
                            $list = array();
                            $data = array('page'        => $page,
                                            'hasNext'   => 'false',
                                            'list'      => $list
                            );
                        }
                    }
                    DB::commit();
                    return Response::json(array(
                        'error'     => false,
                        'code'      => 200,
                        'message'   => 'OK',
                        'data'      => $data
                    ));
                }else{
                    return response('Unauthorized', 401)->header('Content-Type', 'text/plain');
                }

            }catch(Exeption $e){
                Log::info($e);
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => $e->getMessage()
                ));
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            Log::info($validationErrorString);
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }

    public function dismissNotification(){
        $rules = array(
            'apiKey'    => 'required',
            'notificationId' => 'required|exists:notifications,notificationId'
        );
        $validator = Validator::make(Request::all(), $rules);
        if($validator->passes()){
            $apiKey = Request::get('apiKey');
            $notificationId = Request::get('notificationId');

            try{
                DB::beginTransaction();
                $userInfo = $this->apiCtrl->_getCustomerInfo($apiKey);
                if($userInfo){
                    $uid = $userInfo->CUST_NO;
                    DB::table('notifications')->where("notificationId",$notificationId)->where("uid", $uid)->update(array("isDismissed"=>false));
                    DB::commit();
                    return Response::json(array(
                        'error'     => false,
                        'code'      => 200,
                        'message'   => 'OK'
                    ));
                }else{
                    return response('Unauthorized', 401)->header('Content-Type', 'text/plain');
                }
            }catch(Exeption $e){
                DB::rollback();
                Log::info($e);
                return Response::json(array(
                    'error' => true,
                    'code' => 500,
                    'message' => $e->getMessage()
                ));
            }
        }else{
            $validationErrorString = implode(',',$validator->errors()->all());
            Log::info($validationErrorString);
            return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
        }
    }
}
