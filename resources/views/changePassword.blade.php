@extends('layouts.master')
@section('content')
        <!--  <div class="box"> -->
            <div class="box-header">
              <h3 class="page-header" style="margin: 10px 0 5px 0;">Change Password</h3>
            </div>
            <div>
              @if ($errors->has(''))
              <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                  {{ $error }}<br>        
                @endforeach
              </div>
              @elseif(Session::has('success'))
              <div class="alert alert-success" id="success-alert">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                {{ Session::get('success') }}
              </div>
              @elseif(Session::has('fail'))
              <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
              {{ Session::get('fail') }}
              </div>
              @elseif (Session::has('message'))
              <div class='bg-danger alert'>{{ Session::get('message') }}</div>
              @endif            
            </div>
            <form class="form-horizontal" method="POST" id="changePasswordForm" action="{{ asset('api/v1/changePassword') }}">
              <div class="box-body">
                <div class="form-group">
                  <label for="currentPw" class="col-sm-2 control-label">Current Password</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" id="currentPw" name="currentPw" placeholder="Current Password">
                  </div>
                </div>
                <div class="form-group">
                  <label for="newPw" class="col-sm-2 control-label">New Password</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" id="newPw" name="newPw" placeholder="New Password">
                  </div>
                </div>
                <div class="form-group">
                  <label for="confirmPw" class="col-sm-2 control-label">Confirm Password</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" id="confirmPw" name="newPw_confirmation" placeholder="Confirm Password">
                  </div>
                </div>
                <input type="hidden" name="apiKey" value="{{$apiKey}}" />
                <input type="hidden" name="changePasswordNonApi" value="1"/>
              </div>
              <!-- /.box-body -->
              <div class="box-footer changePw">
                <input type="submit" value="Change Password" class="btn btn-default">
              </div>
              <!-- /.box-footer -->
            </form>
      <!--    </div> -->
@stop