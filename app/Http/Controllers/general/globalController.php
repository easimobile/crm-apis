<?php

namespace App\Http\Controllers\general;

use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\UserController as UserController;
use Session;
use DB;

class globalController extends BaseController
{

    public function getApiKey(){
        $apiKey = Session::get('apiKey');
        return $apiKey;
    }

    /* deprecated */
/*    public function getUserCode(){
        $apiKey = Session::get('apiKey');
        $userCode = DB::table('apiAuth')->where('apikey',$apiKey)->value('userId');
        return $userCode;
    }*/

    public function getOutletCode(){
        $this->userCtrl = new UserController();
        $apiKey = Session::get('apiKey');
        $getUserInfoParam = array('apiKey'=>$apiKey);
        $getUserInfoApi = asset('api/v1/getUserInfo');
        $userInfo = $this->userCtrl->getData($getUserInfoApi,$getUserInfoParam);
        $outletCode = $userInfo['outletCode'];

    	return $outletCode;
    }

    public function getApprovalAmount(){
        $this->userCtrl = new UserController();
        $apiKey = Session::get('apiKey');
        $getUserInfoParam = array('apiKey'=>$apiKey);
        $getUserInfoApi = asset('api/v1/getUserInfo');
        $userInfo = $this->userCtrl->getData($getUserInfoApi,$getUserInfoParam);
        $userCode = $userInfo['userCode'];

        $approvalAmount = DB::table('employee')->where('EMP_CD', $userCode)->value('ApprovalAmount');
        return $approvalAmount;
    }

    public function getSysflag($sysflagCode){
        $setting = DB::table('sysflag')->where('flag', $sysflagCode)->value('Setting');
        return $setting;
    }

    public function apiauth(){
        $bytes = openssl_random_pseudo_bytes(64, $cstrong);
        $apikey = bin2hex($bytes);
        
        if(!$cstrong){
            return 0;
        } else {            
            return $apikey;
        }
    }

    public function getLanguage(){
        $data = DB::table('language')->pluck('languageCode');
        return $data;
    }
}